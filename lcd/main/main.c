#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "lcd.h"
#include "font.h"

static void displayTask(lcd_t* lcd){
  point_t point = { .x = 5, .y = 5, };
  lcd->drawString(lcd, "Hello, world", &point);
  lcd->draw(lcd);
  vTaskDelete(NULL);
}

void app_main(void){
  lcd_t* lcd = lcd_new();
  xTaskCreate(displayTask, "display_task", 1024 * 8, lcd, 10, NULL);
  vTaskDelay(1000);
  lcd->free(lcd);
}
