#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "esp_heap_caps.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "font.h"
#include "lcd.h"

uint16_t colors[] = {
    0b0001111100000000 //blue
  , 0b1110000000000111 //green
  , 0b0000000011111000 //red
  , 0b1111111111111111 //white
};

static point_t* drawChar(lcd_t* this, char c, point_t* point);

//free me
static point_t* drawString(lcd_t* this, const char* str, point_t* point){
  point_t* result = NULL;
  while(*str){
    if(NULL != result){
      vPortFree(result);//get rid of all this malloc/free
    }
    result = drawChar(this, *str++, point);
    point->x += this->CHAR_WIDTH;
  }
  return result;
}

static void draw(lcd_t* this){
  esp_lcd_panel_draw_bitmap(this->handle, 0, 0, this->HORIZONTAL_RESOLUTION, this->VERTICAL_RESOLUTION, this->buffer);
}

static void lcd_free(lcd_t* this){
  free(this->buffer);
  esp_lcd_panel_del(this->handle);
  vPortFree(this);
}

static bool transferComplete(esp_lcd_panel_io_handle_t panel_io, esp_lcd_panel_io_event_data_t* edata, void* user_ctx){
  lcd_t* lcd = (lcd_t*)user_ctx;
  xSemaphoreGive(lcd->drawingSemaphore);
  return true;
}

lcd_t* lcd_new(){
  lcd_t _this = {
    .CHAR_HEIGHT           = 8,
    .CHAR_WIDTH            = 5,
    .HOST                  = SPI2_HOST,
//    .PARALLEL_LINES        = 128,
    .PIXEL_CLOCK_HZ        = 10 * 1000 * 1000,
    .BACKLIGHT_OFF         = 1,
    .MOSI_PIN              = 15,
    .CLOCK_PIN             = 16,
    .CS_PIN                = 13,
    .DC_PIN                = 12,
    .RESET_PIN             = 17,
    .BACKLIGHT_PIN         = 5,
    .HORIZONTAL_RESOLUTION = 160,
    .VERTICAL_RESOLUTION   = 128,
    .CMD_BITS              = 8,
    .PARAM_BITS            = 8,
    .drawingSemaphore      = xSemaphoreCreateBinary(),
    .drawString            = drawString,
    .draw                  = draw,
    .free                  = lcd_free,
  };
  lcd_t* this = pvPortMalloc(sizeof(lcd_t));
  memcpy(this, &_this, sizeof(lcd_t));
  int size = this->HORIZONTAL_RESOLUTION * this->VERTICAL_RESOLUTION * sizeof(uint16_t);
    this->buffer = heap_caps_malloc(size, MALLOC_CAP_DMA);
    assert(this->buffer != NULL);
    memset(this->buffer, 0, size);
  //backlight
  gpio_config_t backlight_config = {
    .mode         = GPIO_MODE_OUTPUT,
    .pin_bit_mask = 1ULL << this->BACKLIGHT_PIN,
  };
  ESP_ERROR_CHECK(gpio_config(&backlight_config));
  //setup spi
  spi_bus_config_t spi_config = {
    .sclk_io_num     = this->CLOCK_PIN,
    .mosi_io_num     = this->MOSI_PIN,
    .miso_io_num     = -1,
    .quadwp_io_num   = -1,
    .quadhd_io_num   = -1,
    .max_transfer_sz = size * 2 + 8
  };
  ESP_ERROR_CHECK(spi_bus_initialize(this->HOST, &spi_config, SPI_DMA_CH_AUTO));
  //io
  esp_lcd_panel_io_handle_t io_handle = NULL;
  esp_lcd_panel_io_spi_config_t io_config = {
    .dc_gpio_num         = this->DC_PIN,
    .cs_gpio_num         = this->CS_PIN,
    .pclk_hz             = this->PIXEL_CLOCK_HZ,
    .lcd_cmd_bits        = this->CMD_BITS,
    .lcd_param_bits      = this->PARAM_BITS,
    .spi_mode            = 0,
    .trans_queue_depth   = 10,
    .on_color_trans_done = transferComplete,
    .user_ctx            = this,
  };
  //spi
  ESP_ERROR_CHECK(esp_lcd_new_panel_io_spi((esp_lcd_spi_bus_handle_t)this->HOST, &io_config, &io_handle));
  //panel
  esp_lcd_panel_dev_config_t panel_config = {
    .reset_gpio_num = this->RESET_PIN,
    .color_space    = ESP_LCD_COLOR_SPACE_RGB,
    .bits_per_pixel = 16,
  };
  ESP_ERROR_CHECK(esp_lcd_new_panel_st7789(io_handle, &panel_config, &this->handle));
  //turn off backlight
  ESP_ERROR_CHECK(gpio_set_level(this->BACKLIGHT_PIN, this->BACKLIGHT_OFF));
  //reset, init
  ESP_ERROR_CHECK(esp_lcd_panel_reset(this->handle));
  ESP_ERROR_CHECK(esp_lcd_panel_init(this->handle));

  ESP_ERROR_CHECK(esp_lcd_panel_swap_xy(this->handle, true));
  ESP_ERROR_CHECK(esp_lcd_panel_mirror(this->handle, true, false));

  return this;
}

static void drawPixel(lcd_t* this, point_t* point){
  this->buffer[point->x + (point->y * this->HORIZONTAL_RESOLUTION)] = colors[3];
}

//free me
static point_t* drawChar(lcd_t* this, char c, point_t* point){
  // Convert the character to an index
  c = c & 0x7F;
  if(c < ' '){
    c = 0;
  } else {
    c -= ' ';
  }
  const unsigned char* chr = font[(int)c];
  // Draw pixels and find the last x position and upper y position, so we know how far the "cursor" moved
  point_t result = {
    .x = point->x,
    .y = point->y,
  };
  for(int j = 0; j < this->CHAR_WIDTH; j++){
    for(int i = 0; i < this->CHAR_HEIGHT; i++){
      if(chr[j] & (1 << i)){
        result.x = point->x + j;
        result.y = point->y + i;
        drawPixel(this, &result);
      }
    }
  }
  point_t* _result = pvPortMalloc(sizeof(point_t));
  return _result;
}
