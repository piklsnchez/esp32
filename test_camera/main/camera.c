/*
 * camera.c
 *
 *  Created on: Dec 29, 2021
 *      Author: chris
 */
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_camera.h"
#include "camera.h"

#define CAM_PIN_PWDN 32
#define CAM_PIN_RESET -1 //software reset will be performed
#define CAM_PIN_XCLK 0
#define CAM_PIN_SIOD 26
#define CAM_PIN_SIOC 27
#define CAM_PIN_D7 35
#define CAM_PIN_D6 34
#define CAM_PIN_D5 39
#define CAM_PIN_D4 36
#define CAM_PIN_D3 21
#define CAM_PIN_D2 19
#define CAM_PIN_D1 18
#define CAM_PIN_D0 5
#define CAM_PIN_VSYNC 25
#define CAM_PIN_HREF 23
#define CAM_PIN_PCLK 22

static void takePicture(camera_t* this);
static void startCapture(camera_t* this);
static void freeFrameBuffer(camera_t* this);
static void camera_free(camera_t* this);

camera_t* camera_new() {
  static camera_config_t config = {
    .pin_pwdn     = CAM_PIN_PWDN,
    .pin_reset    = CAM_PIN_RESET,
    .pin_xclk     = CAM_PIN_XCLK,
    .pin_sscb_sda = CAM_PIN_SIOD,
    .pin_sscb_scl = CAM_PIN_SIOC,
    .pin_d7       = CAM_PIN_D7,
    .pin_d6       = CAM_PIN_D6,
    .pin_d5       = CAM_PIN_D5,
    .pin_d4       = CAM_PIN_D4,
    .pin_d3       = CAM_PIN_D3,
    .pin_d2       = CAM_PIN_D2,
    .pin_d1       = CAM_PIN_D1,
    .pin_d0       = CAM_PIN_D0,
    .pin_vsync    = CAM_PIN_VSYNC,
    .pin_href     = CAM_PIN_HREF,
    .pin_pclk     = CAM_PIN_PCLK,
    .xclk_freq_hz = 20000000,
    .ledc_timer   = LEDC_TIMER_0,
    .ledc_channel = LEDC_CHANNEL_0,
    .pixel_format = PIXFORMAT_RGB565, //YUV422,GRAYSCALE,RGB565,JPEG
    .frame_size   = FRAMESIZE_QQVGA, // 160x120   //QQVGA-UXGA Do not use sizes above QVGA when not JPEG
    .jpeg_quality = 12, //0-63 lower number means higher quality
    .fb_count     = 2,       //if more than one, i2s runs in continuous mode. Use only with JPEG
    .grab_mode    = CAMERA_GRAB_WHEN_EMPTY,
  };
  camera_t* this        =  pvPortMalloc(sizeof(camera_t));
  this->config          = NULL; //we don't really need to keep this around
  this->frameBuffer     = NULL;
  this->takePicture     = takePicture;
  this->startCapture    = startCapture;
  this->freeFrameBuffer = freeFrameBuffer;
  this->free            = camera_free;

  // camera init
  esp_err_t err = esp_camera_init(&config);
  if(err != ESP_OK) {
    printf("Camera init failed with error 0x%x", err);
    return NULL;
  }
  ESP_LOGI("camera_new", "camera inited");

  return this;
}

static void camera_free(camera_t* this){
  vPortFree(this);
}

static void takePicture(camera_t* this){
  ESP_LOGI("takePicture", "Taking picture..");
  this->frameBuffer = esp_camera_fb_get();
  if(NULL == this->frameBuffer) {
    ESP_LOGE("takePicture", "Camera capture failed");
  } else {
    ESP_LOGI("takePicture", "framBuffer: %p/n", this->frameBuffer);
  }
}

static  void startCapture(camera_t* this){
  ESP_LOGI("startCapture", "enter");
  while(true){
    this->frameBuffer = esp_camera_fb_get();
    if(NULL == this->frameBuffer) {
      ESP_LOGE("startCapture", "Camera capture failed");
    } else {
      ESP_LOGI("startCapture", "frameBuffer: %p/n", this->frameBuffer);
      esp_camera_fb_return(this->frameBuffer);
    }
  }
}

static void freeFrameBuffer(camera_t* this){
  if(NULL != this->frameBuffer){
    esp_camera_fb_return(this->frameBuffer);
  }
}
