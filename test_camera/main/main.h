#ifndef __MAIN_H
#define __MAIN_H

#define STACK_SIZE          4096

void consoleTask(void* args);
void commandTask(void* args);
void cameraCommand(char* arg);
#endif
