#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "camera.h"
#include "main.h"

camera_t* camera = NULL;
bool firstTime = true;

void consoleTask(void* args){
  ESP_LOGI("consoleTask", "starting");
  while(true){
    if(!firstTime){
      vTaskDelay(1);
      continue;
    }
    firstTime = false;
    cameraCommand("");
  }
  vTaskDelete(NULL);
}

void cameraCommand(char* arg){
  //do camera stuff
  if(NULL == camera){
    camera = camera_new();
    if(NULL == camera){
      ESP_LOGE("cameraCommand", "initing camera failed");
      return;
    }
  }
//  camera->takePicture(camera);
  camera->startCapture(camera);
}

void app_main(){
  ESP_LOGI("app_main", "starting");
  TaskHandle_t consoleTaskHandle;
  xTaskCreate(consoleTask, "console_task", STACK_SIZE, NULL, 10, &consoleTaskHandle);
}
