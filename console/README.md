# ESP32 commandline with wifi and bluetooth
=========================================

- wifi scan
- wifi connect \<sid\> \<password\>

- bluetooth scan
- bluetooth connect \<address\>
