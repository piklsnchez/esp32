#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
//#include "aht.h"
#include "esp_wifi.h"
#include "esp_http_client.h"
#include "esp_event.h"
#include "esp_vfs_dev.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "parseArgs.h"
#include "uartConsole.h"
#include "wifi.h"
//#include "bluetooth.h"
#include "telnetConsole.h"
//#include "i2c.h"
#include "lcdConsole.h"
#include "private/private.h"
#include "main.h"

static console_t*   console   = NULL;
static wifi_t*      wifi      = NULL;
//static bluetooth_t* bluetooth = NULL;
//static i2c_t*       i2c       = NULL;

static void i2cCommand(args_t* args);

void consoleTask(void* args){
  ESP_LOGI("consoleTask", "starting");
  console = uartConsole_new(CONSOLE_UART_PORT_NUM, TXD_PIN, RXD_PIN, CONSOLE_UART_BAUD_RATE);
  //default to telnet console
  wifi    = wifi_singelton(console);
  wifi->connect(SID, PASS);
  console = lcdConsole_new(telnetConsole_new());
  wifi->setConsole(console);
  //end
  commandArgs_t commandArgs = {
    .console = console,
  };
  while(true){
    char* line = console->readline(console);
    if(NULL == line){
      continue;
    }
    strlcpy(commandArgs.command, line, sizeof(commandArgs.command));
    vPortFree(line);
    xTaskCreate(commandTask, "command_task", STACK_SIZE, &commandArgs, 10, NULL);
    vTaskDelay(1);
  }
  console->free(console);
  vTaskDelete(NULL);
}

void commandTask(commandArgs_t* commandArgs){
  args_t* args = parseArgs(commandArgs->command);
  if(0 == strcmp("wifi", args->args[0])){
    if(NULL == wifi){
      wifi = wifi_singelton(console);
    }
    wifiCommand(args);
  } else if(0 == strcmp("bluetooth", args->args[0])){
//    if(NULL == bluetooth){
//      bluetooth = bluetooth_singleton(console);
//    }
    bluetoothCommand(args);
  } else if(0 == strcmp("telnetConsole", args->args[0])){
    if(NULL == wifi){
      wifi = wifi_singelton(console);
    }
    console = telnetConsole_new();
    if(NULL != wifi){
      wifi->setConsole(console);//TODO: all the consoles
    }
  } else if(0 == strcmp("i2c", args->args[0])){
    i2cCommand(args);
  }
  args->free(args);
  vTaskDelete(NULL);
}

void wifiCommand(args_t* args){
  ESP_LOGI("wifiCommand", "args size: %d", args->size);
  if(2 == args->size && 0 == strcmp("scan", args->args[1])){
    wifi->scan(wifi);
  } else if(4 == args->size && 0 == strcmp("connect", args->args[1])){
    wifi->connect(args->args[2], args->args[3]);
  } else if(3 == args->size && 0 == strcmp("get", args->args[1])){
    wifi->get(args->args[2]);
  } else {
    for(int i = 0; i < args->size; i++){
      ESP_LOGI("wifiCommand", "args[%d]: %s", i, args->args[i]);
      for(int j = 0; j < strlen(args->args[i]); j++){
        ESP_LOGI("wifiCommand", "%x", args->args[i][j]);
      }
    }
  }
}

void bluetoothCommand(args_t* args){
//  if(2 == args->size && 0 == strcmp("scan", args->args[1])){
//    bluetooth->scan();
//  } else if(3 == args->size && 0 == strcmp("connect", args->args[1])){
//    console = bluetooth->connect(args->args[2]);
//    //TODO: how will everyone update there reference to console?
//    if(NULL != wifi){
//      wifi->setConsole(console);
//    }
//  }
}

void i2cCommand(args_t* args){
//  if(NULL == i2c){
//    i2c = i2c_new(console);
//  }
//  if(2 == args->size && 0 == strcmp("scan", args->args[1])){
//    i2c->scan(i2c);
//  } else if(2 == args->size && 0 == strcmp("get", args->args[1])){
//    i2c->get(i2c);
//  }
}

void app_main(){
  ESP_LOGI("app_main", "starting");
  //init flash
  ESP_ERROR_CHECK(nvs_flash_init());
  TaskHandle_t consoleTaskHandle;
  xTaskCreate(consoleTask, "console_task", STACK_SIZE, NULL, 10, &consoleTaskHandle);
}
