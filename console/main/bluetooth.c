#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_hidh.h"
#include "esp_err.h"
#include "esp_bt.h"
#include "esp_bt_defs.h"
#include "esp_gatts_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "esp_bt_main.h"
#include "esp_gattc_api.h"
#include "esp_gatt_defs.h"
#include "esp_gap_bt_api.h"
#include "esp_hid_common.h"
#include "console.h"
#include "bluetoothKeyboard.h"
#include "bluetoothConsole.h"
#include "bluetooth.h"
//FYI: GAP = general access profile

#define SIZEOF_ARRAY(a) (sizeof(a)/sizeof(*a))
#define SCAN_DURATION_SECONDS 5

typedef struct scanResult {
  struct scanResult *next;
  esp_bd_addr_t bda;
  const char *name;
  int8_t rssi;
  esp_hid_usage_t usage;
  esp_hid_transport_t transport; //BT, BLE or USB
  union {
    struct {
      esp_bt_cod_t cod;
      esp_bt_uuid_t uuid;
    } bt;
  };
} scanResult_t;

static const char* gapEventNames[] = {
    "DISC_RES"
  , "DISC_STATE_CHANGED"
  , "RMT_SRVCS"
  , "RMT_SRVC_REC"
  , "AUTH_CMPL"
  , "PIN_REQ"
  , "CFM_REQ"
  , "KEY_NOTIF"
  , "KEY_REQ"
  , "READ_RSSI_DELTA"
};

static void          bluetooth_free    (bluetooth_t* this);
static esp_err_t     init              (uint8_t mode);
static esp_err_t     scan              ();
static void          scanTask          ();
static void          scanResultAdd     (esp_bd_addr_t, esp_bt_cod_t*, esp_bt_uuid_t*, uint8_t*, uint8_t, int);
static scanResult_t* scanResultFind    (esp_bd_addr_t bda, scanResult_t* results);
static void          scanResultsFree   (scanResult_t* results);
static console_t*    bluetooth_connect (char* address);
static const char*   getGapEventName   (uint8_t name);
static void          handleDeviceResult(struct disc_res_param*);
static void          handleEvent       (void* this, esp_event_base_t, int32_t, void*);
static void          handleGapEvent    (esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t* param);

static console_t*       console              = NULL;
static scanResult_t*    scanResults          = NULL;
static size_t           scanResultsSize      = 0;
static xSemaphoreHandle handleEventSemaphore = NULL;

bluetooth_t* bluetooth_singleton(console_t* _console){//main
  console = _console;
  bluetooth_t* this = pvPortMalloc(sizeof(bluetooth_t));
  this->free        = bluetooth_free;
  this->scan        = scan;
  this->connect     = bluetooth_connect;
  ESP_ERROR_CHECK(init(ESP_BT_MODE_CLASSIC_BT));//mode
  return this;
}

static void bluetooth_free(bluetooth_t* this){
  vPortFree(this);
}

static esp_err_t init(uint8_t mode){
  if(!mode || mode > ESP_BT_MODE_BTDM){
    ESP_LOGE("init", "Invalid mode given!");
    return ESP_FAIL;
  }

  if(handleEventSemaphore != NULL){
    ESP_LOGE("init", "Already initialized");
    return ESP_FAIL;
  }

  handleEventSemaphore = xSemaphoreCreateBinary();
  if(handleEventSemaphore == NULL){
    ESP_LOGE("init", "xSemaphoreCreateBinary failed!");
    return ESP_FAIL;
  }

  esp_err_t ret;
  esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
  if(mode & ESP_BT_MODE_CLASSIC_BT){
    bt_cfg.mode             = mode;
    bt_cfg.bt_max_acl_conn  = 3;
    bt_cfg.bt_max_sync_conn = 3;
  } else {
    ret = esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT);
    if(ret){
      ESP_LOGE("init", "esp_bt_controller_mem_release failed: %d", ret);
      return ret;
    }
  }

  ret = esp_bt_controller_init(&bt_cfg);
  if(ret){
    ESP_LOGE("init", "esp_bt_controller_init failed: %d", ret);
    return ret;
  }

  ret = esp_bt_controller_enable(mode);
  if(ret){
    ESP_LOGE("init", "esp_bt_controller_enable failed: %d", ret);
    return ret;
  }

  ret = esp_bluedroid_init();
  if(ret){
    ESP_LOGE("init", "esp_bluedroid_init failed: %d", ret);
    return ret;
  }

  ret = esp_bluedroid_enable();
  if(ret){
    ESP_LOGE("init", "esp_bluedroid_enable failed: %d", ret);
    return ret;
  }

  if(mode & ESP_BT_MODE_CLASSIC_BT){
    esp_bt_sp_param_t param_type = ESP_BT_SP_IOCAP_MODE;
    esp_bt_io_cap_t   iocap      = ESP_BT_IO_CAP_IO;
    esp_bt_gap_set_security_param(param_type, &iocap, sizeof(uint8_t));
    // Set default parameters for Legacy Pairing Use fixed pin code
    esp_bt_pin_code_t pin_code = {'1', '2', '3', '4'};
    esp_bt_gap_set_pin(ESP_BT_PIN_TYPE_FIXED, 4, pin_code);

    if((ret = esp_bt_gap_register_callback(handleGapEvent)) != ESP_OK){
      ESP_LOGE("init", "esp_bt_gap_register_callback failed: %d", ret);
      return ret;
    }

    // Allow BT devices to connect back to us
    if((ret = esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_NON_DISCOVERABLE)) != ESP_OK){
      ESP_LOGE("init", "esp_bt_gap_set_scan_mode failed: %d", ret);
      return ret;
    }
  }

  if(ret != ESP_OK){
    vSemaphoreDelete(handleEventSemaphore);
    handleEventSemaphore = NULL;
    return ret;
  }

  esp_hidh_config_t config = {
    .callback = handleEvent,
  };
  ret = esp_hidh_init(&config);
  if(ret != ESP_OK){
    vSemaphoreDelete(handleEventSemaphore);
    handleEventSemaphore  = NULL;
    return ret;
  }

  return ESP_OK;
}

static esp_err_t scan(){
  ESP_LOGI("scanCommand", "enter");
  xTaskCreate(scanTask, "scan_task", 6 * 1024, NULL, 2, NULL);
  return ESP_OK;
}

static void scanTask(){
  ESP_LOGI("scanTask", "enter");
  size_t        results_len = 0;
  scanResult_t* results     = NULL;
  ESP_LOGI("scanTask", "SCAN...");
  //start scan for HID devices
  esp_err_t result;
  if(scanResultsSize || scanResults){
    ESP_LOGE("scan", "There are old scan results. Free them first!");
    result = ESP_FAIL;
  }

  result = esp_bt_gap_start_discovery(ESP_BT_INQ_MODE_GENERAL_INQUIRY, (int)(SCAN_DURATION_SECONDS / 1.28), 0);
  if(ESP_OK == result){
    ESP_LOGI("scan", "take semaphore");
    xSemaphoreTake(handleEventSemaphore, portMAX_DELAY);
  } else {
    ESP_LOGE("scan", "esp_bt_gap_start_discovery failed: %d", result);
  }

  results_len = scanResultsSize;
  results     = scanResults;
  if(scanResultsSize){
    while(scanResults->next != NULL){
      scanResults = scanResults->next;
    }
  }

  scanResultsSize = 0;
  scanResults     = NULL;
  //
  if(ESP_OK != result){
    ESP_LOGE("scan", "scan failure");
  }
  ESP_LOGI("scan", "SCAN: %u results", results_len);
  if(results_len){
    scanResult_t* r = results;
    while(r){
      int length = snprintf(NULL, 0, "name: %s, address: " ESP_BD_ADDR_STR ", transport: %s\r\n"
        , r->name ? r->name : ""
        , ESP_BD_ADDR_HEX(r->bda)
        , r->transport == ESP_HID_TRANSPORT_BT ? "classic" : "other"
      );
      length++;
      char* output = pvPortMalloc(length);
      length = snprintf(output, length, "name: %s, address: " ESP_BD_ADDR_STR ", transport: %s\r\n"
        , r->name ? r->name : ""
        , ESP_BD_ADDR_HEX(r->bda)
        , r->transport == ESP_HID_TRANSPORT_BT ? "classic" : "other"
      );
      console->writeline(console, output);
      vPortFree(output);
      r = r->next;
    }
    //free the results
    scanResultsFree(results);
  }
  vTaskDelete(NULL);
}

static scanResult_t* scanResultFind(esp_bd_addr_t bda, scanResult_t* results){
  scanResult_t* r = results;
  while(r){
    if(memcmp(bda, r->bda, sizeof(esp_bd_addr_t)) == 0){
      return r;
    }
    r = r->next;
  }
  return NULL;
}

static void scanResultAdd( esp_bd_addr_t bda, esp_bt_cod_t* cod, esp_bt_uuid_t* uuid, uint8_t* name, uint8_t name_len, int rssi){
  scanResult_t* r = scanResultFind(bda, scanResults);
  if(r){//results found?
    //Some info may come later
    if(r->name == NULL && name && name_len){
      char* name_s = pvPortMalloc(name_len + 1);
      if(name_s == NULL){
        ESP_LOGE("scanResultAdd", "malloc result name failed!");
        return;
      }
      memcpy(name_s, name, name_len);
      name_s[name_len] = 0;//null terminate
      r->name = (const char*)name_s;//set name
    }
    if(r->bt.uuid.len == 0 && uuid->len){
      memcpy(&r->bt.uuid, uuid, sizeof(esp_bt_uuid_t));//set uuid
    }
    if (rssi != 0) {
      r->rssi = rssi;//set rssi
    }
    return;
  }
  //results weren't found, create them
  r = (scanResult_t*)pvPortMalloc(sizeof(scanResult_t));
  if(r == NULL){
    ESP_LOGE("scanResultAdd", "Malloc bt_hidh_scan_result_t failed!");
    return;
  }
  r->transport = ESP_HID_TRANSPORT_BT;
  memcpy(r->bda,      bda,  sizeof(esp_bd_addr_t));//set bda
  memcpy(&r->bt.cod,  cod,  sizeof(esp_bt_cod_t));//set cod
  memcpy(&r->bt.uuid, uuid, sizeof(esp_bt_uuid_t));//set uuid
  r->usage = esp_hid_usage_from_cod((uint32_t)cod);
  r->rssi  = rssi;//set rssi
  r->name  = NULL;
  if(name_len && name){
    char* name_s = pvPortMalloc(name_len + 1);
    if(name_s == NULL){
      vPortFree(r);
      ESP_LOGE("scanResultAdd", "Malloc result name failed!");
      return;
    }
    memcpy(name_s, name, name_len);
    name_s[name_len] = 0;//null terminate
    r->name = (const char*)name_s;//set name
  }
  r->next     = scanResults;
  scanResults = r;
  scanResultsSize++;
}

static void scanResultsFree(scanResult_t* results){
  scanResult_t* r = NULL;
  while(results){
    r = results;
    results = results->next;
    if(r->name != NULL){
      vPortFree((char*)r->name);
    }
    vPortFree(r);
  }
}

static console_t* bluetooth_connect(char* address){
  ESP_LOGI("connect", "address: %s", address);
  esp_bd_addr_t _address;
  sscanf(address, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &_address[0], &_address[1], &_address[2], &_address[3], &_address[4], &_address[5]);
  esp_hidh_dev_t* ret = esp_hidh_dev_open(_address, ESP_HID_TRANSPORT_BT, 0);
  if(NULL == ret){
    ESP_LOGE("connect", "esp_hidh_dev_open failed");
    return NULL;
  }
  console = bluetoothConsole_new(console);
  return console;
}

static void handleEvent(void* args, esp_event_base_t base, int32_t id, void* event_data){
  esp_hidh_event_t       event = (esp_hidh_event_t)id;
  esp_hidh_event_data_t* param = (esp_hidh_event_data_t*)event_data;

  switch(event){
    case ESP_HIDH_INPUT_EVENT: {
//      uint8_t modifier = param->input.data[0];
      int index = param->input.data[2];
      if(0 != index){
        bluetoothConsole_privateData_t* privateData = (bluetoothConsole_privateData_t*)console->privateData;
        if(42 == index){//backspace
          privateData->buffer[strlen(privateData->buffer) -1] = 0x00;//null terminate the last character
          console->writeline(console, "\b \b");
        } else if(40 == index){//enter '\r'
          console->writeline(console, "\r\n");
          xEventGroupSetBits(privateData->eventGroup, privateData->BUFFER_READY);
        } else if(55 > index){
          char key      = bt_key[index];
          char output[] = {key, 0x00};
          //TODO: maybe this logic belongs somewhere else; check buffer length
          strlcat(privateData->buffer, output, BLUETOOTH_CONSOLE_BUFFER_SIZE);
          console->writeline(console, output);//print to screen
        }
        ESP_LOGI("handleEvent", "index: %i", index);
      }
      break;
    }
    default:
      ESP_LOGI("handleEvent", "EVENT: %d", event);
    break;
  }
}

static void handleGapEvent(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t* param){
  switch (event) {
    case ESP_BT_GAP_DISC_STATE_CHANGED_EVT: {
      ESP_LOGI("handleGapEvent", "BT GAP DISC_STATE %s", (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STARTED) ? "START" : "STOP");
      if (param->disc_st_chg.state == ESP_BT_GAP_DISCOVERY_STOPPED) {
        ESP_LOGI("handleGapEvent", "give semaphore");
        xSemaphoreGive(handleEventSemaphore);
      }
      break;
    }
    case ESP_BT_GAP_DISC_RES_EVT: {
      handleDeviceResult(&param->disc_res);
      break;
    }
    case ESP_BT_GAP_KEY_NOTIF_EVT:
      ESP_LOGI("handleGapEvent", "BT GAP KEY_NOTIF passkey:%d", param->key_notif.passkey);
    break;
    default:
      ESP_LOGV("handleGapEvent", "BT GAP EVENT %s", getGapEventName(event));
    break;
  }
}

static void handleDeviceResult(struct disc_res_param* disc_res){
  uint32_t      codv     = 0;
  esp_bt_cod_t* cod      = (esp_bt_cod_t*)&codv;
  int8_t        rssi     = 0;
  uint8_t*      name     = NULL;
  uint8_t       name_len = 0;
  esp_bt_uuid_t uuid;
  uuid.len         = ESP_UUID_LEN_16;
  uuid.uuid.uuid16 = 0;

  for(int i = 0; i < disc_res->num_prop; i++){
    esp_bt_gap_dev_prop_t* prop = &disc_res->prop[i];
    if(prop->type == ESP_BT_GAP_DEV_PROP_BDNAME){//name
      name     = (uint8_t*)prop->val;
      name_len = strlen((const char*)name);
    } else if(prop->type == ESP_BT_GAP_DEV_PROP_RSSI){//rssi
      rssi = *((int8_t*)prop->val);
    } else if(prop->type == ESP_BT_GAP_DEV_PROP_COD){//cod
      memcpy(&codv, prop->val, sizeof(uint32_t));
    } else if(prop->type == ESP_BT_GAP_DEV_PROP_EIR) {//eir
      uint8_t  len  = 0;
      uint8_t* data = 0;
      //16bit
      data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_CMPL_16BITS_UUID, &len);//complete list 16bit
      if(data == NULL){
        data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_INCMPL_16BITS_UUID, &len);//incomplete list 16bit
      }
      if(data && len == ESP_UUID_LEN_16){
        uuid.len         = ESP_UUID_LEN_16;
        uuid.uuid.uuid16 = data[0] + (data[1] << 8);
        continue;
      }
      //32bit
      data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_CMPL_32BITS_UUID, &len);//complete list 32bit
      if (data == NULL) {
        data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_INCMPL_32BITS_UUID, &len);//incomplete list 32bit
      }
      if (data && len == ESP_UUID_LEN_32) {
        uuid.len = len;
        memcpy(&uuid.uuid.uuid32, data, sizeof(uint32_t));
        continue;
      }
      //128bit
      data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_CMPL_128BITS_UUID, &len);
      if(data == NULL){
        data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_INCMPL_128BITS_UUID, &len);
      }
      if(data && len == ESP_UUID_LEN_128){
        uuid.len = len;
        memcpy(uuid.uuid.uuid128, (uint8_t*)data, len);
        continue;
      }
      //try to find a name
      if(name == NULL){
        data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_CMPL_LOCAL_NAME, &len);
        if(data == NULL){
          data = esp_bt_gap_resolve_eir_data((uint8_t*)prop->val, ESP_BT_EIR_TYPE_SHORT_LOCAL_NAME, &len);
        }
        if(data && len){
          name     = data;
          name_len = len;
        }
      }
    }
  }
//mouse keyboard etc.
  if(cod->major == ESP_BT_COD_MAJOR_DEV_PERIPHERAL || (scanResultFind(disc_res->bda, scanResults) != NULL)) {
    scanResultAdd(disc_res->bda, cod, &uuid, name, name_len, rssi);
  }
}

static const char* getGapEventName(uint8_t event){
  if(event >= SIZEOF_ARRAY(gapEventNames)){
    return "UNKNOWN";
  }
  return gapEventNames[event];
}
