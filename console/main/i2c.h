/*
 * i2c.h
 *
 *  Created on: Nov 27, 2021
 *      Author: chris
 */

#ifndef MAIN_I2C_H_
#define MAIN_I2C_H_

#include "aht.h"
#include "console.h"

struct i2c {
  aht_t* aht;
  console_t* console;
  void (*scan)(struct i2c* this);
  void (*get)(struct i2c* this);
  void (*free)(struct i2c* this);
} typedef i2c_t;

i2c_t* i2c_new(console_t* this);

#endif /* MAIN_I2C_H_ */
