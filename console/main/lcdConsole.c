/*
 * lcdConsole.c
 *
 *  Created on: Dec 22, 2021
 *      Author: chris
 */
#include "lcd.h"
#include "lcdConsole.h"

static char* lcdConsole_readline(console_t* this);
static void  lcdConsole_writeline(console_t* this, char* data);
static void  lcdConsole_free(console_t* this);

console_t* lcdConsole_new(console_t* console){
  console_t* this = pvPortMalloc(sizeof(console_t));
  this->free        = lcdConsole_free;
  this->readline    = lcdConsole_readline;
  this->writeline   = lcdConsole_writeline;
  this->privateData = pvPortMalloc(sizeof(lcdConsole_privateData_t));
  ((lcdConsole_privateData_t*)this->privateData)->console = console;
  ((lcdConsole_privateData_t*)this->privateData)->lcd = lcd_new();
  return this;
}

static char* lcdConsole_readline(console_t* this){
  lcdConsole_privateData_t* privateData = (lcdConsole_privateData_t*)this->privateData;
  return privateData->console->readline(privateData->console);
}

static void lcdConsole_writeline(console_t* this, char* data){
  lcdConsole_privateData_t* privateData = (lcdConsole_privateData_t*)this->privateData;
  privateData->console->writeline(privateData->console, data);
  privateData->lcd->drawString(privateData->lcd, data);
}

static void lcdConsole_free(console_t* this){
  lcd_t* lcd = ((lcdConsole_privateData_t*)this->privateData)->lcd;
  lcd->free(lcd);
  vPortFree(this->privateData);
  vPortFree(this);
}
