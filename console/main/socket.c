/*
 * socket.c
 *
 *  Created on: Nov 24, 2021
 *      Author: chris
 */

#include <sys/socket.h>
#include "esp_log.h"
#include "socket.h"

const int SOCKET_PORT        = 1234;
const int SOCKET_BACKLOG_LEN = 4;
const int SOCKET_BUFFER_LEN  = 1024;

static int   socket_bind  (socket_t* this);
static int   socket_listen(socket_t* this);
static int   socket_accept(socket_t* this, struct sockaddr_in* address);
static char* socket_read  (socket_t* this);
static void  socket_write (socket_t* this, char* data);
static void  socket_free  (socket_t* this);

socket_t* socket_new(){
  int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  return socket_new1(fd);
}

socket_t* socket_new1(int fd){
  socket_t* this                      = pvPortMalloc(sizeof(socket_t));
  this->fd                            = fd;
  this->serverAddress.sin_family      = AF_INET;
  this->serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
  this->serverAddress.sin_port        = htons(SOCKET_PORT);
  this->_buffer                       = (char*)calloc(SOCKET_BUFFER_LEN+1, sizeof(char));//TODO: get rid of the calloc
  this->bind                          = socket_bind;
  this->listen                        = socket_listen;
  this->accept                        = socket_accept;
  this->read                          = socket_read;
  this->write                         = socket_write;
  this->free                          = socket_free;
  return this;
}

static int socket_bind(socket_t* this){
  return bind(this->fd, (struct sockaddr*)&this->serverAddress, sizeof(this->serverAddress));
}

static int socket_listen(socket_t* this){
  return listen(this->fd, SOCKET_BACKLOG_LEN);
}

static int socket_accept(socket_t* this, struct sockaddr_in* address){
  socklen_t len = sizeof(*address);
  return accept(this->fd, (struct sockaddr*)address, &len);
}

static char* socket_read(socket_t* this){
  ESP_LOGI("socket_read", "enter");
  int r = read(this->fd, this->_buffer, SOCKET_BUFFER_LEN);
  ESP_LOGI("socket_read", "read %i bytes", r);
  this->_buffer[r] = '\0';
  return this->_buffer;
}

static void socket_write(socket_t* this, char* data){
  write(this->fd, data, strlen(data));
}

static void socket_free(socket_t* this){
  free(this->_buffer);
  close(this->fd);
  free(this);
}
