/*
 * bluetoothConsole.c
 *
 *  Created on: Nov 20, 2021
 *      Author: chris
 */
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "console.h"
#include "socket.h"
#include "bluetoothConsole.h"

static char* readline (console_t* this);
static void  writeline(console_t* this, char* data);
static void  bluetoothConsole_free(console_t* this);

console_t* bluetoothConsole_new(console_t* console){
  console_t* this = pvPortMalloc(sizeof(console_t));
  this->free      = bluetoothConsole_free;
  this->readline  = readline;
  this->writeline = writeline;
  bluetoothConsole_privateData_t _privateData = {
    .console      = console,
    .BUFFER_READY = BIT0,
    .eventGroup   = xEventGroupCreate(),
  };
  bluetoothConsole_privateData_t* privateData = pvPortMalloc(sizeof(bluetoothConsole_privateData_t));
  memcpy(privateData, &_privateData, sizeof(bluetoothConsole_privateData_t));
  this->privateData = privateData;

  return this;
}

static char* readline(console_t* this){
  bluetoothConsole_privateData_t* privateData = (bluetoothConsole_privateData_t*)this->privateData;
  xEventGroupWaitBits(privateData->eventGroup, privateData->BUFFER_READY, pdTRUE, pdFALSE, portMAX_DELAY);
  //TODO: semaphore to guard these few steps
  char* result = pvPortMalloc(sizeof(privateData->buffer));
  strlcpy(result, privateData->buffer, BLUETOOTH_CONSOLE_BUFFER_SIZE);
  memset(privateData->buffer, 0, BLUETOOTH_CONSOLE_BUFFER_SIZE);
  return result;
}

static void writeline(console_t* this, char* data){
  bluetoothConsole_privateData_t* privateData = (bluetoothConsole_privateData_t*)this->privateData;
  privateData->console->writeline(privateData->console, data);
  if(NULL != privateData->displayClient){
    if(-1 != privateData->displayClient->fd){
      privateData->displayClient->write(privateData->displayClient, data);
    } else {
      privateData->displayClient->free(privateData->displayClient);
      privateData->displayClient = NULL;
    }
  }
}

static void bluetoothConsole_free(console_t* this){
  vPortFree(this->privateData);
  vPortFree(this);
}
