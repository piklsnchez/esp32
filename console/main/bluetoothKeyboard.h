/*
 * bluetoothKeyboard.h
 *
 *  Created on: Nov 10, 2021
 *      Author: chris
 */

#ifndef MAIN_BLUETOOTHKEYBOARD_H_
#define MAIN_BLUETOOTHKEYBOARD_H_
static char bt_key[] = {
    '?'
  , '?'
  , '?'
  , '?'
  , 'a'
  , 'b'//5
  , 'c'
  , 'd'
  , 'e'
  , 'f'
  , 'g'//10
  , 'h'
  , 'i'
  , 'j'
  , 'k'
  , 'l'//15
  , 'm'
  , 'n'
  , 'o'
  , 'p'
  , 'q'//20
  , 'r'
  , 's'
  , 't'
  , 'u'
  , 'v'//25
  , 'w'
  , 'x'
  , 'y'
  , 'z'
  , '1'//30
  , '2'
  , '3'
  , '4'
  , '5'
  , '6'//35
  , '7'
  , '8'
  , '9'
  , '0'
  , '\r'//40
  , '?'
  , '\b'//backspace
  , '?'
  , ' '
  , '?'//45
  , '?'
  , '?'
  , '?'
  , '?'
  , '?'//50
  , '?'
  , '?'
  , '?'//esc
  , '?'
  , '?'//55
};


#endif /* MAIN_BLUETOOTHKEYBOARD_H_ */
