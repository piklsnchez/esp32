#ifndef __MAIN_H
#define __MAIN_H

#define TXD_PIN GPIO_NUM_15
#define RXD_PIN GPIO_NUM_14

#define CONSOLE_UART_PORT_NUM   UART_NUM_1
#define CONSOLE_UART_BAUD_RATE  115200

#define STACK_SIZE          4096
#define MAX_COMMAND_SIZE    128
#define MAX_ARGS_SIZE       5
#define MAX_OUTPUT_SIZE     4096
#define WIFI_SCAN_LIST_SIZE 10
#define WIFI_CONNECTED_BIT  BIT0
#define WIFI_FAIL_BIT       BIT1

struct string{
  int length;
  char* string;
};

struct commandArgs {
  char command[MAX_COMMAND_SIZE];
  console_t* console;
} typedef commandArgs_t;

void consoleTask(void* args);
void commandTask(commandArgs_t* args);
void wifiCommand(args_t* args);
void bluetoothCommand(args_t* args);
#endif
