/*
 * socket.h
 *
 *  Created on: Nov 24, 2021
 *      Author: chris
 */

#ifndef MAIN_SOCKET_H_
#define MAIN_SOCKET_H_

#include <sys/socket.h>

struct socket{
  int fd;
  struct sockaddr_in serverAddress;
  char* _buffer;
  int (*bind)(struct socket* this);
  int (*listen)(struct socket* this);
  int (*accept)(struct socket* this, struct sockaddr_in* address);
  char* (*read)(struct socket* this);
  void (*write)(struct socket* this, char* data);
  void (*free)(struct socket* this);
} typedef socket_t;

struct socket* socket_new();
struct socket* socket_new1(int fd);

#endif /* MAIN_SOCKET_H_ */
