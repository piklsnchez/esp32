/*
 * i2c.c
 *
 *  Created on: Nov 27, 2021
 *      Author: chris
 */
#include <stdbool.h>
#include <string.h>
#include "driver/i2c.h"
#include "aht.h"
#include "freertos/FreeRTOS.h"
#include "esp_err.h"
#include "esp_log.h"
#include "console.h"
#include "i2c.h"

#define SDA_PIN 13
#define SCL_PIN 16

static void i2c_free(i2c_t* this);
static void i2c_scan(i2c_t* this);
static void i2c_get(i2c_t* this);
static void scanTask(void* this);
static void sensorTask(void* this);

i2c_t* i2c_new(console_t* _console){
  ESP_LOGI("i2c_new", "console: %p", _console);
  i2c_t* this   = pvPortMalloc(sizeof(i2c_t));
  this->aht     = NULL;
  this->console = _console;
  this->free    = i2c_free;
  this->scan    = i2c_scan;
  this->get     = i2c_get;

  return this;
}

static void i2c_free(i2c_t* this){
  ESP_LOGI("i2c_free", "enter");
  if(NULL == this->aht){
    vPortFree(this->aht);
  }
  vPortFree(this);
}

static void i2c_scan(i2c_t* this){
  ESP_LOGI("i2c_scan", "enter");
  xTaskCreate(scanTask, "scan_task", 6 * 1024, this, 2, NULL);
}

static void i2c_get(i2c_t* this){
  ESP_LOGI("i2c_get", "this: %p", this);
  xTaskCreate(sensorTask,"get_task", 6 * 1024, this, 2, NULL);
}

void sensorTask(void* _this){
  i2c_t* this = (i2c_t*)_this;
  ESP_LOGI("sensorTask", "enter");
  if(NULL == this->aht){
    ESP_ERROR_CHECK(i2cdev_init());
    aht_t dev = { 0 };
    dev.mode = AHT_MODE_NORMAL;
    dev.type = AHT_TYPE_AHT1x;

    ESP_ERROR_CHECK(aht_init_desc(&dev, 0x38, 0, SDA_PIN, SCL_PIN));
    ESP_ERROR_CHECK(aht_init(&dev));
    this->aht = pvPortMalloc(sizeof(aht_t));
    memcpy(this->aht, &dev, sizeof(aht_t));
  }

  bool calibrated;
  ESP_ERROR_CHECK(aht_get_status(this->aht, NULL, &calibrated));
  if(calibrated){
    ESP_LOGI("sensorTask", "Sensor calibrated");
  } else {
    ESP_LOGW("sensorTask", "Sensor not calibrated!");
  }
  float temperature;
  float humidity;

  esp_err_t res = aht_get_data(this->aht, &temperature, &humidity);
  if(res == ESP_OK){
    char buf[50];
    snprintf(buf, 50, "Temperature: %.1f°C, Humidity: %.2f%%\r\n", temperature, humidity);
    this->console->writeline(this->console, buf);
  } else {
    ESP_LOGE("sensorTask", "Error reading data: %d (%s)", res, esp_err_to_name(res));
  }
  vTaskDelete(NULL);
}

void scanTask(void* _this){
  i2c_t* this = (i2c_t*)_this;
  ESP_LOGD("scanTask", "enter");
  if(NULL == this->aht){
    i2c_config_t conf = {
      .mode             = I2C_MODE_MASTER,
      .sda_io_num       = SDA_PIN,
      .scl_io_num       = SCL_PIN,
      .sda_pullup_en    = GPIO_PULLUP_ENABLE,
      .scl_pullup_en    = GPIO_PULLUP_ENABLE,
      .master.clk_speed = 100000,
    };
    esp_err_t error = i2c_param_config(I2C_NUM_0, &conf);
    i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0);
  }
  esp_err_t error;
  this->console->writeline(this->console, "     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\r\n");
  this->console->writeline(this->console, "00:         ");
  for (int i = 3; i < 0x78; i++) {
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (i << 1) | I2C_MASTER_WRITE, 1 /* expect ack */);
    i2c_master_stop(cmd);

    error = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10 / portTICK_PERIOD_MS);
    if(i % 16 == 0){
      char buf[10];
      snprintf(buf, 10, "\r\n%.2x:", i);
      this->console->writeline(this->console, buf);
    }
    if(error == 0){
      char buf[10];
      snprintf(buf, 10, " %.2x", i);
      this->console->writeline(this->console, buf);
    } else {
      this->console->writeline(this->console, " --");
    }
    //ESP_LOGD(tag, "i=%d, rc=%d (0x%x)", i, espRc, espRc);
    i2c_cmd_link_delete(cmd);
  }
  this->console->writeline(this->console, "\r\n");
  if(NULL == this->aht){
    i2c_driver_delete(I2C_NUM_0);
  }
  vTaskDelete(NULL);
}
