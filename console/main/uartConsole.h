#ifndef MAIN_UARTCONSOLE_H_
#define MAIN_UARTCONSOLE_H_
#include "console.h"

#define RTS_PIN  UART_PIN_NO_CHANGE
#define CTS_PIN  UART_PIN_NO_CHANGE

struct uartConsole_privateData {
  const int BUFFER_LENGTH;
  const int MAX_COMMAND_LENGTH;
  int uartNumber;
  int txPin;
  int rxPin;
  int rtsPin;
  int ctsPin;
  int baudRate;
  int length;
} typedef uartConsole_privateData_t;

//implement console_t
console_t* uartConsole_new(int uartNumber, int txPin, int rxPin, int baudRate);
char* uartConsole_readline(console_t* this);
void uartConsole_writeline(console_t* this, char* data);
void uartConsole_free(console_t* this);

#endif /* MAIN_UARTCONSOLE_H_ */
