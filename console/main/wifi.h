/*
 * wifi.h
 *
 *  Created on: Nov 1, 2021
 *      Author: chris
 */

#ifndef MAIN_WIFI_H_
#define MAIN_WIFI_H_

#include "freertos/event_groups.h"

struct wifi {
  void (*free)(struct wifi* this);
  void (*scan)();
  void (*connect)(char* ssid, char* password);
  void (*get)(char* url);
  void (*setConsole)(console_t* console);
  char ipaddress[16];
} typedef wifi_t;

wifi_t* wifi_singelton();
#endif /* MAIN_WIFI_H_ */
