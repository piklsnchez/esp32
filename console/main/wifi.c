#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_http_client.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_tls.h"
#include "esp_crt_bundle.h"
#include "uartConsole.h"
#include "wifi.h"

static void wifi_free(wifi_t* this);
static void wifi_scan();
static void wifi_connect(char* ssid, char* password);
static void wifi_get(char* url);
static void wifi_setConsole(console_t* _console);
static void wifi_wifiEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
static void wifi_ipEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
static esp_err_t wifi_httpEvent(esp_http_client_event_t* event);
static void wifi_get_ssl(const char* url);

static const int SCAN_LIST_SIZE = 10;
static const int CONNECTED_BIT  = BIT0;
static const int FAIL_BIT       = BIT1;
static console_t* console;
static EventGroupHandle_t eventGroup;

wifi_t* wifi_singelton(console_t* _console){
  console = _console;
  eventGroup = xEventGroupCreate();
  wifi_t* this     = pvPortMalloc(sizeof(wifi_t));
  this->free       = wifi_free;
  this->scan       = wifi_scan;
  this->connect    = wifi_connect;
  this->get        = wifi_get;
  this->setConsole = wifi_setConsole;
  this->ipaddress[0] = 0x00;

  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());
  esp_netif_t* sta_netif = esp_netif_create_default_wifi_sta();
  assert(sta_netif);
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
  esp_event_handler_instance_t instance_any_id;
  esp_event_handler_instance_t instance_got_ip;
  ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID   , &wifi_wifiEvent, this, &instance_any_id));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT  , IP_EVENT_STA_GOT_IP, &wifi_ipEvent  , this, &instance_got_ip));
  ESP_ERROR_CHECK(esp_wifi_start());
  return this;
}

static void wifi_free(wifi_t* this){
  if(NULL != this->ipaddress){
    vPortFree(this->ipaddress);
  }
  vPortFree(this);
}

static void wifi_scan(){
  uint16_t         number = SCAN_LIST_SIZE;
  wifi_ap_record_t ap_info[SCAN_LIST_SIZE];
  uint16_t         ap_count = 0;
  memset(ap_info, 0, sizeof(ap_info));
  esp_wifi_scan_start(NULL, true);
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));
  int length = snprintf(NULL, 0, "Total APs scanned = %u\r\n", ap_count);
  length++;
  char* output = pvPortMalloc(length);
  length = snprintf(output, length, "Total APs scanned = %u\r\n", ap_count);
  console->writeline(console, output);
  vPortFree(output);
  for(int i = 0; (i < SCAN_LIST_SIZE) && (i < ap_count); i++){
    length = snprintf(NULL, 0, "SSID \t\t%s\r\nRSSI \t\t%d\r\nChannel \t\t%d\r\n", ap_info[i].ssid, ap_info[i].rssi, ap_info[i].primary);
    length++;
    output = pvPortMalloc(length);
    length = snprintf(output, length, "SSID \t\t%s\r\nRSSI \t\t%d\r\nChannel \t\t%d\r\n", ap_info[i].ssid, ap_info[i].rssi, ap_info[i].primary);
    console->writeline(console, output);
    vPortFree(output);
  }
}

static void wifi_connect(char* ssid, char* password){
  wifi_config_t staConf = {
    .sta = {
      .threshold.authmode = WIFI_AUTH_WPA2_PSK,
      .pmf_cfg = {
        .capable = true,
        .required = false
      },
    },
  };
  strlcpy((char*)staConf.sta.ssid    , ssid, sizeof(staConf.sta.ssid));
  strlcpy((char*)staConf.sta.password, password, sizeof(staConf.sta.password));
  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &staConf));
  ESP_ERROR_CHECK(esp_wifi_connect());
  // Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed (WIFI_FAIL_BIT). The bits are set by event_handler() (see above)
  EventBits_t bits = xEventGroupWaitBits(eventGroup, CONNECTED_BIT | FAIL_BIT, pdFALSE, pdFALSE, portMAX_DELAY);
  if(bits & CONNECTED_BIT){
    int length = snprintf(NULL, 0, "connected to ap SSID:%s password:%s\r\n", staConf.sta.ssid, staConf.sta.password);
    length++;//null
    char* output = pvPortMalloc(length);
    length = snprintf(output, length, "connected to ap SSID:%s password:%s\r\n", staConf.sta.ssid, staConf.sta.password);
    console->writeline(console, output);
    vPortFree(output);
    ESP_LOGI("wifiCommand", "connected");
  } else if (bits & FAIL_BIT) {
    ESP_LOGI("wifiCommand", "Failed to connect");
  } else {
    ESP_LOGE("wifiCommand", "UNEXPECTED EVENT");
  }
}

static void wifi_get(char* url){
  if(NULL == strstr(url, "https://")){
    esp_http_client_config_t config = {
      .url                   = url,
      .method                = HTTP_METHOD_GET,
      .disable_auto_redirect = false,
      .event_handler         = wifi_httpEvent,
    };
    ESP_LOGI("wifiCommand", "get: %s", config.url);
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t                result = esp_http_client_perform(client);
    if (result == ESP_OK) {
    } else {
      ESP_LOGE("wifiCommand", "error: (%d)", result);
    }
    esp_http_client_cleanup(client);
  } else {
    wifi_get_ssl((const char*)url);
  }
}

static void wifi_get_ssl(const char* url){
  ESP_LOGI("wifi_get_ssl", "enter");
  esp_tls_cfg_t cfg = {
    .crt_bundle_attach = esp_crt_bundle_attach,
  };
  char buf[512];
  int ret;
  int len;

  struct esp_tls* tls = esp_tls_conn_http_new(url, &cfg);

  if (tls != NULL) {
    ESP_LOGI("wifi_get_ssl", "Connection established...");
  } else {
    ESP_LOGE("wifi_get_ssl", "Connection failed...");
    esp_tls_conn_delete(tls);
    return;
  }
  int length = snprintf(
    NULL, 0, "GET %s HTTP/1.1\r\nUser-Agent: esp-idf/1.0 esp32\r\n\r\n", url);
  length++;
  char request[length];
  snprintf(request, length, "GET %s HTTP/1.1\r\nUser-Agent: esp-idf/1.0 esp32\r\n\r\n", url);

  size_t written_bytes = 0;
  do {
    ret = esp_tls_conn_write(tls, request + written_bytes, strlen(request) - written_bytes);
    if (ret >= 0) {
      ESP_LOGI("wifi_get_ssl", "%d bytes written", ret);
      written_bytes += ret;
    } else if (ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
      ESP_LOGE("wifi_get_ssl", "esp_tls_conn_write  returned: [0x%02X](%s)", ret, esp_err_to_name(ret));
      esp_tls_conn_delete(tls);
      return;
    }
  } while (written_bytes < strlen(request));
  ESP_LOGI("wifi_get_ssl", "Reading HTTP response...");

  do {
    len = sizeof(buf) - 1;
    bzero(buf, sizeof(buf));
    ret = esp_tls_conn_read(tls, (char *)buf, len);
    if (ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ) {
      continue;
    }

    if (ret < 0) {
      ESP_LOGE("wifi_get_ssl", "esp_tls_conn_read  returned [-0x%02X](%s)", -ret, esp_err_to_name(ret));
      break;
    }

    if (ret == 0) {
      ESP_LOGI("wifi_get_ssl", "connection closed");
      break;
    }

    len = ret;
    ESP_LOGD("wifi_get_ssl", "%d bytes read", len);
    console->writeline(console, buf);
  } while (1);
}



static void wifi_setConsole(console_t* _console){
  console = _console;
}

static void wifi_wifiEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data){
  ESP_LOGI("wifiEvent", "event_base: %s, event_id: %d", event_base, event_id);
  switch(event_id){
    case WIFI_EVENT_WIFI_READY:
      ESP_LOGI("wifiEvent", "wifi ready");
    break;
    case WIFI_EVENT_SCAN_DONE:
      ESP_LOGI("wifiEvent", "finished scanning");
    break;
    case WIFI_EVENT_STA_START:
      ESP_LOGI("wifiEvent", "station start");
    break;
    case WIFI_EVENT_STA_STOP:
      ESP_LOGI("wifiEvent", "station stop");
    break;
    case WIFI_EVENT_STA_CONNECTED:
      ESP_LOGI("wifiEvent", "station connected");
    break;
    case WIFI_EVENT_STA_DISCONNECTED:
      ESP_LOGI("wifiEvent", "station disconnected");
    break;
    case WIFI_EVENT_STA_AUTHMODE_CHANGE:
      ESP_LOGI("wifiEvent", "authmode changed");
    break;
  }
}

static void wifi_ipEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data){
  ESP_LOGI("ipEvent", "event_base: %s, event_id: %d", event_base, event_id);
  wifi_t* this = (wifi_t*)arg;
  switch(event_id){
    case IP_EVENT_STA_GOT_IP:
      snprintf(this->ipaddress, sizeof(this->ipaddress), IPSTR, IP2STR(&((ip_event_got_ip_t*)event_data)->ip_info.ip));
      ESP_LOGI("ipEvent", "got ip: %s", this->ipaddress);
      console->writeline(console, this->ipaddress);
      xEventGroupSetBits(eventGroup, CONNECTED_BIT);
    break;
  }
}

static esp_err_t wifi_httpEvent(esp_http_client_event_t* event){
  switch(event->event_id) {
    case HTTP_EVENT_ERROR:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ERROR");
    break;
    case HTTP_EVENT_ON_CONNECTED:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_CONNECTED");
    break;
    case HTTP_EVENT_HEADER_SENT:
      ESP_LOGI("httpEvent", "HTTP_EVENT_HEADER_SENT");
    break;
    case HTTP_EVENT_ON_HEADER:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_HEADER");
      int length = snprintf(NULL, 0, "%s: %s\r\n", event->header_key, event->header_value);
      length++;
      char* output = pvPortMalloc(length);
      length = snprintf(output, length, "%s: %s\r\n", event->header_key, event->header_value);
      console->writeline(console, output);
      vPortFree(output);
    break;
    case HTTP_EVENT_ON_DATA:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_DATA, len=%d", event->data_len);
      console->writeline(console, event->data);
    break;
    case HTTP_EVENT_ON_FINISH:
      ESP_LOGI("http_event", "HTTP_EVENT_ON_FINISH");
    break;
    case HTTP_EVENT_DISCONNECTED:
      ESP_LOGI("http_event", "HTTP_EVENT_DISCONNECTED");
    break;
  }
  return ESP_OK;
}
