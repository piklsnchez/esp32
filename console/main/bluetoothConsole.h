/*
 * bluetoothConsole.h
 *
 *  Created on: Nov 20, 2021
 *      Author: chris
 */

#ifndef MAIN_BLUETOOTHCONSOLE_H_
#define MAIN_BLUETOOTHCONSOLE_H_

#include "freertos/event_groups.h"
#include "socket.h"
#include "console.h"

#define BLUETOOTH_CONSOLE_BUFFER_SIZE 1024

struct bluetoothConsole_privateData {
  socket_t* displayClient;
  console_t* console;
  char buffer[BLUETOOTH_CONSOLE_BUFFER_SIZE];
  const int BUFFER_READY;
  EventGroupHandle_t eventGroup;
} typedef bluetoothConsole_privateData_t;


//implement console_t
console_t* bluetoothConsole_new(console_t* console);

#endif /* MAIN_BLUETOOTHCONSOLE_H_ */
