/*
 * bluetooth.h
 *
 *  Created on: Nov 3, 2021
 *      Author: chris
 */

#ifndef MAIN_BLUETOOTH_H_
#define MAIN_BLUETOOTH_H_

#include "esp_err.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_gattc_api.h"
#include "esp_gatt_defs.h"
#include "esp_gap_bt_api.h"
#include "esp_hid_common.h"
#include "console.h"

typedef struct bluetooth {
  void       (*free)(struct bluetooth* this);
  esp_err_t  (*scan)();
  console_t* (*connect)(char* address);
} bluetooth_t;

bluetooth_t* bluetooth_singleton(console_t* console);

#endif /* MAIN_BLUETOOTH_H_ */
