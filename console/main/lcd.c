#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "esp_heap_caps.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "font.h"
#include "lcd.h"

uint16_t colors[] = {
    0b0001111100000000 //blue
  , 0b1110000000000111 //green
  , 0b0000000011111000 //red
  , 0b1111111111111111 //white
  , 0b0000000000000000 //black
};

static void drawChar(lcd_t* this, char c);

static void drawString(lcd_t* this, const char* str){
  while(*str){
    drawChar(this, *str++);
  }
  this->draw(this);
}

static void draw(lcd_t* this){//TODO: we don't need to draw the whole screen everytime
  esp_lcd_panel_draw_bitmap(this->handle, 0, 0, this->HORIZONTAL_RESOLUTION, this->VERTICAL_RESOLUTION, this->buffer);
}

static void drawPixel(lcd_t* this, point_t* point){
  this->buffer[point->x + (point->y * this->HORIZONTAL_RESOLUTION)] = colors[3];
}

static void clearPixel(lcd_t* this, point_t* point){
  this->buffer[point->x + (point->y * this->HORIZONTAL_RESOLUTION)] = colors[4];
}

//free me
static void drawChar(lcd_t* this, char c){
//  ESP_LOGI("drawChar", "c: %c, x: %i, y: %i", c, this->cursorPosition.x, this->cursorPosition.y);
  // Convert the character to an index
  char _c = c & 0x7F;
  if(_c < ' '){
    _c = 0;
  } else {
    _c -= ' ';
  }
  const unsigned char* chr = font[(int)_c];
  if('\n' == c || this->cursorPosition.x + this->CHAR_WIDTH >= this->HORIZONTAL_RESOLUTION){
    this->cursorPosition.x = 0;
    if(this->cursorPosition.y + this->CHAR_HEIGHT >= this->VERTICAL_RESOLUTION){
      ESP_LOGI("drawChar", "scroll");
      //scroll
      uint32_t t = xTaskGetTickCount();
      ESP_LOGI("drawChar", "waiting at %u", t);
      xSemaphoreTake(this->drawingSemaphore, portMAX_DELAY);
      t = xTaskGetTickCount() - t;
      ESP_LOGI("drawChar", "waited %u for semaphore", t);
      memmove(
          this->buffer
        , this->buffer + (this->HORIZONTAL_RESOLUTION * this->CHAR_HEIGHT)
        , this->BUFFER_SIZE - (this->HORIZONTAL_RESOLUTION * this->CHAR_HEIGHT)
      );
      memset(
          this->buffer + (this->cursorPosition.y * this->HORIZONTAL_RESOLUTION)
        , 0x00
        , this->HORIZONTAL_RESOLUTION * this->CHAR_HEIGHT * 2//bytes
      );
      this->draw(this);
    } else {
      this->cursorPosition.y += this->CHAR_HEIGHT;
    }
  }
  // Draw pixels
//  ESP_LOGI("drawChar", "c: %c, x: %i, y: %i", '\n' == c || '\r' == c ? '^' : c, this->cursorPosition.x, this->cursorPosition.y);
  for(int j = 0; j < this->CHAR_WIDTH; j++){
    for(int i = 0; i < this->CHAR_HEIGHT; i++){
      point_t point = { .x = this->cursorPosition.x + j, .y = this->cursorPosition.y + i };
      if(chr[j] & (1 << i)){
        drawPixel(this, &point);
      } else {
        clearPixel(this, &point);
      }
    }
  }
  if('\n' != c && '\r' != c){//TODO: move this up
    this->cursorPosition.x += this->CHAR_WIDTH;
  }
}

static void lcd_free(lcd_t* this){
  heap_caps_free(this->buffer);
  esp_lcd_panel_del(this->handle);
  vPortFree(this);
}

static bool transferComplete(esp_lcd_panel_io_handle_t panel_io, esp_lcd_panel_io_event_data_t* edata, void* user_ctx){
  lcd_t* lcd = (lcd_t*)user_ctx;
  xSemaphoreGive(lcd->drawingSemaphore);
  return true;
}

lcd_t* lcd_new(){
  lcd_t _this = {
    .CHAR_HEIGHT           = 8,
    .CHAR_WIDTH            = 5,
    .HOST                  = SPI2_HOST,
//    .PARALLEL_LINES        = 128,
    .PIXEL_CLOCK_HZ        = 10 * 1000 * 1000,
    .BACKLIGHT_OFF         = 1,
    .MOSI_PIN              = 15,
    .CLOCK_PIN             = 16,
    .CS_PIN                = 13,
    .DC_PIN                = 12,
    .RESET_PIN             = 17,
//    .BACKLIGHT_PIN         = -1,
    .HORIZONTAL_RESOLUTION = 160,
    .VERTICAL_RESOLUTION   = 128,
    .CMD_BITS              = 8,
    .PARAM_BITS            = 8,
    .BUFFER_SIZE           = 160 * 128 * sizeof(uint16_t),
    .NUMBER_OF_BUFFERS     = 1,
    .cursorPosition        = {0,0},
    .drawingSemaphore      = xSemaphoreCreateBinary(),
    .drawString            = drawString,
    .draw                  = draw,
    .free                  = lcd_free,
  };
  lcd_t* this = pvPortMalloc(sizeof(lcd_t));
  memcpy(this, &_this, sizeof(lcd_t));
  this->buffer = heap_caps_malloc(this->BUFFER_SIZE, MALLOC_CAP_DMA);
  assert(this->buffer != NULL);
  memset(this->buffer, 0x00, this->BUFFER_SIZE);
  //backlight
//  gpio_config_t backlight_config = {
//    .mode         = GPIO_MODE_OUTPUT,
//    .pin_bit_mask = 1ULL << this->BACKLIGHT_PIN,
//  };
//  ESP_ERROR_CHECK(gpio_config(&backlight_config));
  //setup spi
  spi_bus_config_t spi_config = {
    .sclk_io_num     = this->CLOCK_PIN,
    .mosi_io_num     = this->MOSI_PIN,
    .miso_io_num     = -1,
    .quadwp_io_num   = -1,
    .quadhd_io_num   = -1,
    .max_transfer_sz = (this->BUFFER_SIZE * 2) + 8
  };
  ESP_ERROR_CHECK(spi_bus_initialize(this->HOST, &spi_config, SPI_DMA_CH_AUTO));
  //io
  esp_lcd_panel_io_handle_t io_handle = NULL;
  esp_lcd_panel_io_spi_config_t io_config = {
    .dc_gpio_num         = this->DC_PIN,
    .cs_gpio_num         = this->CS_PIN,
    .pclk_hz             = this->PIXEL_CLOCK_HZ,
    .lcd_cmd_bits        = this->CMD_BITS,
    .lcd_param_bits      = this->PARAM_BITS,
    .spi_mode            = 0,
    .trans_queue_depth   = 10,
    .on_color_trans_done = transferComplete,
    .user_ctx            = this,
  };
  //spi
  ESP_ERROR_CHECK(esp_lcd_new_panel_io_spi((esp_lcd_spi_bus_handle_t)this->HOST, &io_config, &io_handle));
  //panel
  esp_lcd_panel_dev_config_t panel_config = {
    .reset_gpio_num = this->RESET_PIN,
    .color_space    = ESP_LCD_COLOR_SPACE_RGB,
    .bits_per_pixel = 16,
  };
  ESP_ERROR_CHECK(esp_lcd_new_panel_st7789(io_handle, &panel_config, &this->handle));
  //turn off backlight
//  ESP_ERROR_CHECK(gpio_set_level(this->BACKLIGHT_PIN, this->BACKLIGHT_OFF));
  //reset, init
  ESP_ERROR_CHECK(esp_lcd_panel_reset(this->handle));
  ESP_ERROR_CHECK(esp_lcd_panel_init(this->handle));

  ESP_ERROR_CHECK(esp_lcd_panel_swap_xy(this->handle, true));
  ESP_ERROR_CHECK(esp_lcd_panel_mirror(this->handle, true, false));
  this->draw(this);//blank screen
  xSemaphoreTake(this->drawingSemaphore, portMAX_DELAY);
  return this;
}
