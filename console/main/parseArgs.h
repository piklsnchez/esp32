/*
 * parseArgs.h
 *
 *  Created on: Oct 29, 2021
 *      Author: chris
 */

#ifndef MAIN_PARSEARGS_H_
#define MAIN_PARSEARGS_H_
struct args {
  char** args;
  int size;
  const int MAX_ARGS_SIZE;// = 5;
  const int MAX_ARG_LENGTH;// = 64;
  void (*free)(struct args*);
} typedef args_t;

args_t* args_new();
void args_free(args_t* this);
args_t* parseArgs(char* line);

#endif /* MAIN_PARSEARGS_H_ */
