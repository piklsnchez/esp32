/*
 * telnetConsole.c
 *
 *  Created on: Nov 25, 2021
 *      Author: chris
 *      implements console
 */
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "socket.h"
#include "console.h"
#include "telnetConsole.h"

static void  telnetConsole_free(console_t* this);
static void  telnetConsole_writeline(console_t* this, char* data);
static char* telnetConsole_readline(console_t* this);
static void  telnetConsole_connectionTask(console_t* this);

static const int BUFFER_SIZE = 1024;

console_t* telnetConsole_new(){
  console_t* this = pvPortMalloc(sizeof(console_t));
  this->free      = telnetConsole_free;
  this->writeline = telnetConsole_writeline;
  this->readline  = telnetConsole_readline;
  telnetConsole_privateData_t* privateData = pvPortMalloc(sizeof(telnetConsole_privateData_t));
  this->privateData = privateData;
  privateData->buffer = pvPortMalloc(BUFFER_SIZE);
  privateData->clientSocket = NULL;
  xTaskCreate(telnetConsole_connectionTask, "connection_task", 6 * 1024, this, 2, NULL);
  return this;
}

static void telnetConsole_free(console_t* this){
  telnetConsole_privateData_t* privateData = (telnetConsole_privateData_t*)this->privateData;
  vPortFree(privateData->buffer);
  vPortFree(privateData);
  vPortFree(this);
}

static void telnetConsole_writeline(console_t* this, char* data){
  telnetConsole_privateData_t* privateData = (telnetConsole_privateData_t*)this->privateData;
  if(NULL != privateData->clientSocket){
    privateData->clientSocket->write(privateData->clientSocket, data);
  }
}

static char* telnetConsole_readline(console_t* this){
  telnetConsole_privateData_t* privateData = (telnetConsole_privateData_t*)this->privateData;
  if(NULL == privateData->clientSocket){
    vTaskDelay(1);
    return NULL;
  }
  char* read   = privateData->clientSocket->read(privateData->clientSocket);
  char* result = pvPortMalloc(strlen(read));
  read[strlen(read) -1] = 0;//\r
  strlcpy(result, read, strlen(read));
  return result;
}

static void telnetConsole_connectionTask(console_t* this){
  ESP_LOGI("telnetConsole_connectionTask", "enter");
  telnetConsole_privateData_t* privateData = (telnetConsole_privateData_t*)this->privateData;
  socket_t* sock = socket_new();
  sock->bind(sock);
  sock->listen(sock);

  struct sockaddr_in clientAddress;
  while(true){
    vTaskDelay(1);
    if(NULL == privateData->clientSocket){
      ESP_LOGI("telnetConsole_connectionTask", "waiting for connection");
      vTaskDelay(1);
      privateData->clientSocket = socket_new1(sock->accept(sock, &clientAddress));
      char buff[128];
      ESP_LOGI("telnetConsole_connectionTask", "connected to: %s", esp_ip4addr_ntoa((esp_ip4_addr_t*)&clientAddress.sin_addr, buff, 128));
    }
  }
  sock->free(sock);
}
