/*
 * telnetConsole.h
 *
 *  Created on: Nov 25, 2021
 *      Author: chris
 */

#ifndef MAIN_TELNETCONSOLE_H_
#define MAIN_TELNETCONSOLE_H_

#include "socket.h"
#include "console.h"//implement

struct telnetConsole_privateData {
  socket_t* clientSocket;
  char* buffer;
} typedef telnetConsole_privateData_t;

console_t* telnetConsole_new();
#endif /* MAIN_TELNETCONSOLE_H_ */
