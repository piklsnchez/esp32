/*
 * lcd_console.h
 *
 *  Created on: Dec 22, 2021
 *      Author: chris
 */

#ifndef MAIN_LCDCONSOLE_H_
#define MAIN_LCDCONSOLE_H_

#include "lcd.h"
#include "console.h"

struct lcdConsole_privateData {
  console_t* console;
  lcd_t* lcd;
} typedef lcdConsole_privateData_t;

//implement console_t
console_t* lcdConsole_new(console_t* console);

#endif /* MAIN_LCDCONSOLE_H_ */
