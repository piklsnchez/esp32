/*
 * console.h
 *
 *  Created on: Nov 20, 2021
 *      Author: chris
 */

#ifndef MAIN_CONSOLE_H_
#define MAIN_CONSOLE_H_
struct console {//interface
  char* (*readline)(struct console*);
  void (*writeline)(struct console*, char*);
  void (*free)(struct console*);
  void* privateData;
} typedef console_t;


#endif /* MAIN_CONSOLE_H_ */
