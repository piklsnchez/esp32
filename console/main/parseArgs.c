#include <stdbool.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "parseArgs.h"

args_t* args_new(){
  args_t _this = {//pvPortMalloc(sizeof(args_t));
    .MAX_ARGS_SIZE  = 5,
    .MAX_ARG_LENGTH = 64,
    .free           = args_free,
    .size           = 0,
  };
  _this.args = pvPortMalloc(sizeof(char*) * _this.MAX_ARGS_SIZE);
  args_t* this = pvPortMalloc(sizeof(args_t));
  memcpy(this, &_this, sizeof(args_t));
  return this;
}

void args_free(args_t* this){
  for(int i = 0; i < this->size; i++){
    vPortFree(this->args[i]);
  }
  vPortFree(this->args);
  vPortFree(this);
}

args_t* parseArgs(char* line){
  ESP_LOGI("parseArgs", "line: %s", line);
  bool escape  = false;
  int length   = strlen(line);
  args_t* args = args_new();
  char* tmpArg = pvPortMalloc(sizeof(char*) * args->MAX_ARG_LENGTH);
  int pos = 0;
  for(int i = 0; i < length; i++){
    if('"' == line[i]){
      escape = !escape;
      continue;
    }
    if(!escape && ' ' == line[i]){
      args->args[args->size] = pvPortMalloc(sizeof(char) * (pos + 1));
      strlcpy(args->args[args->size], tmpArg, pos + 1);//fill in arg[i] and increment size; +1 for NULL
      args->size++;
      pos = 0;
    } else {
      tmpArg[pos] = line[i];
      pos++;
    }
  }
  args->args[args->size] = pvPortMalloc(sizeof(char) * (pos + 1));
  strlcpy(args->args[args->size++], tmpArg, pos + 1);//fill in arg[i] and increment size; +1 for NULL
  vPortFree(tmpArg);
  return args;
}
