#include <stdbool.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "console.h"
#include "uartConsole.h"

console_t* uartConsole_new(int uartNumber, int txPin, int rxPin, int baudRate){
  uartConsole_privateData_t _privateData = {
      .MAX_COMMAND_LENGTH = 128,
      .BUFFER_LENGTH      = 1024,
      .uartNumber         = uartNumber,
      .txPin              = txPin,
      .rxPin              = rxPin,
      .rtsPin             = RTS_PIN,
      .ctsPin             = CTS_PIN,
      .baudRate           = baudRate,
      .length             = 0,
  };
  uartConsole_privateData_t* privateData = pvPortMalloc(sizeof(uartConsole_privateData_t));
  memcpy(privateData, &_privateData, sizeof(uartConsole_privateData_t));
  console_t* this   = pvPortMalloc(sizeof(console_t));
  this->readline    = uartConsole_readline;
  this->writeline   = uartConsole_writeline;
  this->free        = uartConsole_free;
  this->privateData = privateData;
  /* Configure parameters of an UART driver, communication pins and install the driver */
  uart_config_t uart_config = {
      .baud_rate  = privateData->baudRate
    , .data_bits  = UART_DATA_8_BITS
    , .parity     = UART_PARITY_DISABLE
    , .stop_bits  = UART_STOP_BITS_1
    , .flow_ctrl  = UART_HW_FLOWCTRL_DISABLE
    , .source_clk = UART_SCLK_APB
  };
  ESP_ERROR_CHECK(
    uart_driver_install(
        privateData->uartNumber
      , privateData->BUFFER_LENGTH
      , privateData->BUFFER_LENGTH
      , 0
      , NULL
      , 0
    )
  );
  ESP_ERROR_CHECK(
    uart_param_config(
        privateData->uartNumber
      , &uart_config
    )
  );
  ESP_ERROR_CHECK(
    uart_set_pin(
        privateData->uartNumber
      , privateData->txPin
      , privateData->rxPin
      , privateData->rtsPin
      , privateData->ctsPin
    )
  );
  return this;
}

void uartConsole_free(console_t* this){
  vPortFree(this->privateData);
  vPortFree(this);
}

char* uartConsole_readline(console_t* this){//TODO: check max length
  uartConsole_privateData_t* privateData = (uartConsole_privateData_t*)this->privateData;
  char buf;
  int len = 0;
  char* line = pvPortMalloc(sizeof(char*) * privateData->MAX_COMMAND_LENGTH);
  privateData->length = 0;
  while(true){
    while(0 == (len = uart_read_bytes(privateData->uartNumber, &buf, 1, 20 / portTICK_RATE_MS))){}
    //read a command upto \r
    if('\r' == buf){
      uart_write_bytes(privateData->uartNumber, "\r\n", 2);
      line[privateData->length] = 0x00;
//      ESP_LOGI("readline_read", "line: %s", line);
      return line;
    } else if('\n' == buf){//do nothing
    } else if(127 == buf){//backspace
      char backspace = '\b';
      uart_write_bytes(privateData->uartNumber, &backspace, 1);
      if(privateData->length > 0){
        privateData->length--;
      }
    } else {
      line[privateData->length++] = buf;
      uart_write_bytes(privateData->uartNumber, &buf, 1);
    }
  }
  return line;
}

void uartConsole_writeline(console_t* this, char* data){
  uart_write_bytes(((uartConsole_privateData_t*)this->privateData)->uartNumber, data, strlen(data));
}
