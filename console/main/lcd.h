/*
 * lcd.h
 *
 *  Created on: Dec 20, 2021
 *      Author: chris
 */

#ifndef MAIN_LCD_H_
#define MAIN_LCD_H_

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "esp_heap_caps.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

//move me
struct point {
  uint8_t x;
  uint8_t y;
} typedef point_t;

struct lcd {
  const int CHAR_HEIGHT;
  const int CHAR_WIDTH;
  const int HOST;
//  const int PARALLEL_LINES;
  const int PIXEL_CLOCK_HZ;
  const int BACKLIGHT_OFF;
  const int MOSI_PIN;
  const int CLOCK_PIN;
  const int CS_PIN;
  const int DC_PIN;
  const int RESET_PIN;
  const int BACKLIGHT_PIN;
  const int HORIZONTAL_RESOLUTION;
  const int VERTICAL_RESOLUTION;
  const int CMD_BITS;
  const int PARAM_BITS;
  const int BUFFER_SIZE;
  const int NUMBER_OF_BUFFERS;
  point_t cursorPosition;
  SemaphoreHandle_t drawingSemaphore;
  esp_lcd_panel_handle_t handle;
//  uint16_t* lines[2];
  uint16_t* buffer;
  void (*drawString)(struct lcd* this, const char* string);
  void (*draw)(struct lcd* this);
  void (*free)(struct lcd* this);
} typedef lcd_t;

lcd_t* lcd_new();

#endif /* MAIN_LCD_H_ */
