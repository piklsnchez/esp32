#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/udp.h>
#include <iostream>
#include <string>
#include <cstring>
#include "UdpClient.hpp"

UdpClient::UdpClient(ip_addr_t* address, int port){  
  ip_set_option(this->pcb, SOF_BROADCAST);
}

UdpClient::~UdpClient(){
  free(this->address);
  udp_disconnect(this->pcb);
  udp_remove(this->pcb);
  this->pcb = NULL;
}

//IP_ADDR_BROADCAST
err_t UdpClient::send(std::string& message){
  //debug
  //printf("enter send %p, %s\n", this, message);
  struct pbuf* buffer = pbuf_alloc(PBUF_TRANSPORT, message.length(), PBUF_RAM);
  memcpy(buffer->payload, message.c_str(), message.length());
  err_t result = udp_sendto(this->pcb, buffer, this->address, this->port);
  if(ERR_OK != result){
    std::cerr << "ERROR: udp_sendto failed" << std::endl;
  }
  
  pbuf_free(buffer);
  return result;
}
