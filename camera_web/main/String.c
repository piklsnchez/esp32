#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include "String.h"

bool String_equals(char* string, char* other){
  return 0 == strcmp(string, other);
}

int String_length(char* string){
  return strlen(string);
}

char* String_format(char* format, ...){
  va_list vaList;
  va_start(vaList, format);
  size_t size = vsnprintf(NULL, 0, format, vaList);
  size++;
  char* result = malloc(size);
  vsnprintf(result, size, format, vaList);
  va_end(vaList);
  return result;
}

char* String_concat(char* string, const char* concat){
  int sourceLength = String_length(string);
  int concatLength = String_length((char*)concat);
  int totalLength  = sourceLength + concatLength + 1;
  char* result     = malloc(totalLength);
  strncpy(result, string, totalLength);
  strncat(result, concat, concatLength);
  return result;
}

bool String_startsWith(char* string, const char* search){
  for(int i = 0; i < String_length((char*)search); i++){
    if(string[i] != search[i]){
      return false;
    }
  }
  return true;
}

int String_indexOf(char* string, const char* search){
  char* found = strstr(string, search);
  return NULL == found ? -1 : found - string;
}

bool String_contains(char* string, const char* search){
  return NULL != strstr(string, search);
}

//start inclusive, end exclusive
char* String_subString(char* string, int start, int end){
  int length     = end - start;
  char* result   = malloc(length + 1);
  strncpy(result, string + start, length);
  result[length] = '\0';
  return result;
}

char* String_copy(char* string){
  int length   = String_length(string);
  char* result = malloc(length + 1);
  strcpy(result, string);
  return result;
}

char** String_split(char* string, char split){
  int    stringLength = String_length(string);
  int    length = 0;
  char** result = NULL;
  int    start  = 0;
  for(int i = 0; i < stringLength; i++){
    if(string[i] == split){
      //realloc result for one extra spot for string pointer
      result = realloc(result, sizeof(char*) * (length + 1));
      //allocate new string from
      result[length] = String_subString(string, start, i);
      start = i +1;
      length++;
    }
  }
  //put the rest in the end
  result = realloc(result, sizeof(char*) * (length + 1));
  result[length] = String_subString(string, start, stringLength);
  return result;
}
