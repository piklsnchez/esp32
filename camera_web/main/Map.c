#include <esp_log.h>
#include <string.h>
#include <stdio.h>
#include "String.h"
#include "Map.h"

//TODO: you must free the values yourself
static void Map_free(Map_t* this){
  for(int i = 0; i < MAP_SIZE; i++){
    char* key = this->pairs[i].key;
    if(NULL != key){
      free(key);
    }
  }
  free(this);
}

static void Map_put(Map_t* this, const char* key, void* value){
  ESP_LOGI("put", "enter");
  for(int i = 0; i < MAP_SIZE - 1; i++){
    printf("%d \n", i);
    if(NULL == this->pairs[i].key){
      this->pairs[i].key   = String_copy((char*)key);
      this->pairs[i].value = value;
      printf("%d key: %s(%s), value: %p(%p)\n", i, key, this->pairs[i].key, value, this->pairs[i].value);
      this->pairs[i + 1].key = NULL;
      break;
    }
  }
}

static void* Map_get(Map_t* this, char* key){
  ESP_LOGI("get", "enter");
  for(int i = 0; i < MAP_SIZE && NULL != this->pairs[i].key; i++){
    char* keyString = this->pairs[i].key;
    printf("search: %s, key: %s\n", key, keyString);
    if(NULL != keyString && String_equals(keyString, key)){
      return this->pairs[i].value;
    }
  }
  return NULL;
}

Map_t* Map_new(){
  Map_t _this = {
    .free = Map_free,
    .put  = Map_put,
    .get  = Map_get,
  };
  Map_t* this = malloc(sizeof(_this));
  memcpy(this, &_this, sizeof(_this));
  this->pairs[0].key = NULL;
  return this;
}
