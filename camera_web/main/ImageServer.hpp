#pragma once

#include <esp_wifi.h>
#include <esp_http_server.h>
#include <string>
#include "Wifi.hpp"
#include "Camera.hpp"

class ImageServer {
  public:
  Wifi   wifi;
  Camera camera;
  httpd_handle_t handle;
  ImageServer(Wifi& wifi, Camera& camera);
  ~ImageServer();
  httpd_handle_t startWebServer();
  void           stopWebServer();
};
