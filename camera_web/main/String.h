#ifndef __STRING_H_
#define __STRING_H_
#ifdef __cplusplus
extern "C" {
#endif
#include <stdbool.h>
#include <stdarg.h>

bool   String_equals    (char* string, char* other);
int    String_length    (char* string);
char*  String_format    (char* format, ...);
char*  String_concat    (char* string, const char* concat);
bool   String_startsWith(char* string, const char* search);
int    String_indexOf   (char* string, const char* search);
bool   String_contains  (char* string, const char* search);
char*  String_subString (char* string, int start, int end);
char*  String_copy      (char* string);
char** String_split     (char* string, char split);

char* String_vformat(char* format, va_list vaList);
#ifdef __cplusplus
}
#endif
#endif
