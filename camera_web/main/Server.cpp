#include <string.h>
#include <esp_log.h>
#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/tcp.h>
#include <any>
#include <map>
#include <string>
#include <sstream>
#include "String.h"
#include "ImageServer.hpp"
#include "RequestHandler.hpp"
#include "Server.hpp"

const char* Server_errorString[] = {
  "Ok.",                    /* ERR_OK          0  */
  "Out of memory error.",   /* ERR_MEM        -1  */
  "Buffer error.",          /* ERR_BUF        -2  */
  "Timeout.",               /* ERR_TIMEOUT    -3  */
  "Routing problem.",       /* ERR_RTE        -4  */
  "Operation in progress.", /* ERR_INPROGRESS -5  */
  "Illegal value.",         /* ERR_VAL        -6  */
  "Operation would block.", /* ERR_WOULDBLOCK -7  */
  "Address in use.",        /* ERR_USE        -8  */
  "Already connecting.",    /* ERR_ALREADY    -9  */
  "Already connected.",     /* ERR_ISCONN     -10 */
  "Not connected.",         /* ERR_CONN       -11 */
  "Low-level netif error.", /* ERR_IF         -12 */
  "Connection aborted.",    /* ERR_ABRT       -13 */
  "Connection reset.",      /* ERR_RST        -14 */
  "Connection closed.",     /* ERR_CLSD       -15 */
  "Illegal argument."       /* ERR_ARG        -16 */
};

static err_t Server_receive_callback(void* arg, struct tcp_pcb* pcb, struct pbuf* buffer, err_t error){
  ESP_LOGI("receive_callback", "enter");
  printf("pcb: %p\n", pcb);

  Server* server = reinterpret_cast<Server*>(arg);
  if(NULL == buffer){//remote has closed the connection
    delete server;
    return ERR_OK;
  }
  server->pcb         = pcb;
  std::string request(reinterpret_cast<char*>(buffer->payload), static_cast<size_t>(buffer->len));
  server->doRequest(request);
  tcp_recved(server->pcb, buffer->len);
  pbuf_free(buffer);
  return ERR_OK;
}

static void Server_error_callback(void* arg, err_t error){
  printf("ERROR: %s\n", Server_errorString[-error]);

  Server* server = reinterpret_cast<Server*>(arg);
  delete server;
}

static err_t Server_poll_callback(void* arg, struct tcp_pcb* pcb){
  Server* server = reinterpret_cast<Server*>(arg);
  server->pcb = pcb;
  if(server->parent->clientCount != server->parent->previousClientCount){
    printf("previous clients: %d, current clients: %d\n", server->parent->previousClientCount, server->parent->clientCount);
    server->parent->previousClientCount = server->parent->clientCount;
  }
  return ERR_OK;
}

static err_t Server_sent_callback(void* arg, struct tcp_pcb* pcb, u16_t length){
  ESP_LOGI("sent_callback", "enter");
  printf("pcb: %p\n", pcb);

  Server* server = reinterpret_cast<Server*>(arg);
  server->pcb = pcb;
  return ERR_OK;
}

static err_t Server_accept_callback(void* arg, struct tcp_pcb* pcb, err_t error){
  ESP_LOGI("accept_callback", "enter");
  printf("pcb: %p\n", pcb);

  Server* server = reinterpret_cast<Server*>(arg);
  server->clientCount++;
  Server* client = new Server();//TODO: don't create a full blown server object, just create a client object
  client->parent = server;
  client->pcb    = pcb;
  tcp_arg (pcb, client);
  tcp_recv(pcb, Server_receive_callback);
  tcp_err (pcb, Server_error_callback);
  tcp_poll(pcb, Server_poll_callback, 0);
  tcp_sent(pcb, Server_sent_callback);
  return ERR_OK;
}

Server::Server(): Server(nullptr){}

Server::Server(RequestHandler* requestHandler){
	this->requestHandler      = requestHandler;
	this->clientCount         = 0;
	this->previousClientCount = 0;
}

Server::~Server(){
  tcp_close(this->pcb);
  this->pcb = NULL;
  this->parent->clientCount--;
}

err_t Server::bind(){
  ESP_LOGI("bind", "enter");
  return tcp_bind(this->pcb, IP_ADDR_ANY, 81);
}

err_t Server::listen(){
  ESP_LOGI("listen", "enter");
  err_t error = ERR_OK;
  this->pcb = tcp_listen(this->pcb);
  if(NULL == this->pcb){
    fprintf(stderr, "ERROR: listen\n");
    return error;
  }
  printf("state: %s\n", tcp_debug_state_str(this->pcb->state));
  return ERR_OK;
}

err_t Server::accept(){
  tcp_accept(this->pcb, Server_accept_callback);
  return ERR_OK;
}

err_t Server::doRequest(std::string& request){
	ESP_LOGI("doRequest", "enter");
	std::any anyThis(this);
	this->parent->requestHandler->handleRequest(request, anyThis);
	return ERR_OK;
}

err_t Server::start(){
  ESP_LOGI("start", "enter");
  this->pcb = tcp_new();
  err_t error = this->bind();
  if(ERR_OK != error){
    if(ERR_USE == error){
      fprintf(stderr, "ERROR: bind port already in nuse\n");
      return error;
    }
    if(ERR_VAL == error){
      fprintf(stderr, "ERROR: bind invalid state\n");
      return error;
    }
    fprintf(stderr, "ERROR: bind(%d)\n", error);
    return error;
  }

  error =  this->listen();
  if(ERR_OK != error){
    fprintf(stderr, "ERROR: listen(%d)\n", error);
    return error;
  }

  tcp_arg(this->pcb, this);
  error = this->accept();
  if(ERR_OK != error){
    fprintf(stderr, "ERROR: accept\n");
    return error;
  }

  return ERR_OK;
}

err_t Server::stop(){
  return ERR_OK;
}
