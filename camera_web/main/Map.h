#ifndef _MAP_H
#define _MAP_H

#define MAP_SIZE 25 

struct Pair {
  char* key;
  void* value;
} typedef Pair_t;

struct Map {
  Pair_t pairs[MAP_SIZE];
  void  (*free)(struct Map* this);
  void  (*put)(struct Map* this, const char* key, void* value);
  void* (*get)(struct Map* this, char* key);
} typedef Map_t;

Map_t* Map_new();
#endif
