#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <esp_wifi.h>
#include <esp_http_client.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_tls.h>
#include <esp_crt_bundle.h>
#include <nvs_flash.h>
#include <iostream>
#include <string>
#include <memory>
#include <cstring>
#include <cstdio>
#include "private/private.h"
#include "Wifi.hpp"

static const int SCAN_LIST_SIZE = 10;
static const int CONNECTED_BIT  = BIT0;
static const int FAIL_BIT       = BIT1;
static EventGroupHandle_t eventGroup;

static void Wifi_wifiEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data){
  ESP_LOGI("wifiEvent", "event_base: %s, event_id: %d", event_base, event_id);
  
  switch(event_id){
    case WIFI_EVENT_WIFI_READY:
      ESP_LOGI("wifiEvent", "wifi ready");
    break;
    case WIFI_EVENT_SCAN_DONE:
      ESP_LOGI("wifiEvent", "finished scanning");
    break;
    case WIFI_EVENT_STA_START:
      ESP_LOGI("wifiEvent", "station start");
    break;
    case WIFI_EVENT_STA_STOP:
      ESP_LOGI("wifiEvent", "station stop");
    break;
    case WIFI_EVENT_STA_CONNECTED:
      ESP_LOGI("wifiEvent", "station connected");
    break;
    case WIFI_EVENT_STA_DISCONNECTED:
      ESP_LOGI("wifiEvent", "station disconnected");
    break;
    case WIFI_EVENT_STA_AUTHMODE_CHANGE:
      ESP_LOGI("wifiEvent", "authmode changed");
    break;
  }
}

static void Wifi_ipEvent(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data){
  ESP_LOGI("ipEvent", "event_base: %s, event_id: %d", event_base, event_id);

  Wifi* thiz = reinterpret_cast<Wifi*>(arg);
  switch(event_id){
    case IP_EVENT_STA_GOT_IP:
      std::snprintf(thiz->ipaddress, sizeof(thiz->ipaddress), IPSTR, IP2STR(&(reinterpret_cast<ip_event_got_ip_t*>(event_data))->ip_info.ip));
      std::cout << "got ip: " << thiz->ipaddress << std::endl;
      xEventGroupSetBits(eventGroup, CONNECTED_BIT);
    break;
  }
}

esp_err_t Wifi_httpEvent(esp_http_client_event_t* event){
  switch(event->event_id) {
    case HTTP_EVENT_ERROR:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ERROR");
    break;
    case HTTP_EVENT_ON_CONNECTED:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_CONNECTED");
    break;
    case HTTP_EVENT_HEADER_SENT:
      ESP_LOGI("httpEvent", "HTTP_EVENT_HEADER_SENT");
    break;
    case HTTP_EVENT_ON_HEADER:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_HEADER");
      std::cout << event->header_key << ": " << event->header_value << "\r" << std::endl;
    break;
    case HTTP_EVENT_REDIRECT:
      ESP_LOGI("httpEvent", "HTTP_EVENT_REDIRECT");
    break;
    case HTTP_EVENT_ON_DATA:
      ESP_LOGI("httpEvent", "HTTP_EVENT_ON_DATA, len=%d", event->data_len);
      std::cout << event->data << std::endl;
    break;
    case HTTP_EVENT_ON_FINISH:
      ESP_LOGI("http_event", "HTTP_EVENT_ON_FINISH");
    break;
    case HTTP_EVENT_DISCONNECTED:
      ESP_LOGI("http_event", "HTTP_EVENT_DISCONNECTED");
    break;
  }
  return ESP_OK;
}

Wifi::Wifi(){
  eventGroup = xEventGroupCreate();

  ESP_ERROR_CHECK(nvs_flash_init());
  ESP_ERROR_CHECK(esp_netif_init());
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  esp_netif_t* sta_netif = esp_netif_create_default_wifi_sta();
  assert(sta_netif);

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));
  ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));

  esp_event_handler_instance_t instance_any_id;
  esp_event_handler_instance_t instance_got_ip;
  ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID   , Wifi_wifiEvent, this, &instance_any_id));
  ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT  , IP_EVENT_STA_GOT_IP, Wifi_ipEvent  , this, &instance_got_ip));

  ESP_ERROR_CHECK(esp_wifi_start());
}

void Wifi::scan(){
  uint16_t         number = SCAN_LIST_SIZE;
  wifi_ap_record_t ap_info[SCAN_LIST_SIZE];
  uint16_t         ap_count = 0;
  std::memset(ap_info, 0, sizeof(ap_info));

  esp_wifi_scan_start(nullptr, true);
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&number, ap_info));
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&ap_count));
  std::cout << "Total APs scanned = " << ap_count << "\r" << std::endl;
  for(int i = 0; (i < SCAN_LIST_SIZE) && (i < ap_count); i++){
    std::cout << "SSID \t\t" << ap_info[i].ssid << "\r\nRSSI \t\t" << ap_info[i].rssi << "\r\nChannel \t\t" << ap_info[i].primary << "\r" << std::endl;
  }
}

void Wifi::connect(){
  wifi_config_t staConf = {
    .sta = {
      .pmf_cfg = {
        .capable  = true,
        .required = false
      },
    },
  };
  staConf.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;

  std::string wifi_ssid (WIFI_SSID);
  for(int i = 0; i < wifi_ssid.length(); i++){
    staConf.sta.ssid[i] = wifi_ssid[i];
  }
  
  std::string wifi_password (WIFI_PASSWORD);
  for(int i = 0; i < wifi_password.length(); i++){
    staConf.sta.password[i] = wifi_password[i];
  }

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
  ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &staConf));
  ESP_ERROR_CHECK(esp_wifi_connect());

  // Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed (WIFI_FAIL_BIT). The bits are set by event_handler() (see above)
  EventBits_t bits = xEventGroupWaitBits(eventGroup, CONNECTED_BIT | FAIL_BIT, pdFALSE, pdFALSE, portMAX_DELAY);
  if(bits & CONNECTED_BIT){
    std::cout << "connected to ap SSID:" << staConf.sta.ssid << " password:" << staConf.sta.password << "\r" << std::endl;
    ESP_LOGI("wifiCommand", "connected");
  } else if (bits & FAIL_BIT) {
    ESP_LOGI("wifiCommand", "Failed to connect");
  } else {
    ESP_LOGE("wifiCommand", "UNEXPECTED EVENT");
  }
}

void Wifi::get(std::string& url){
  if(0 != url.find("https://")){//not starts with
    esp_http_client_config_t config = {
      .url                   = url.c_str(),
      .method                = HTTP_METHOD_GET,
      .disable_auto_redirect = false,
      .event_handler         = Wifi_httpEvent,
    };
    ESP_LOGI("wifiCommand", "get: %s", config.url);
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t                result = esp_http_client_perform(client);
    if (result == ESP_OK) {
    } else {
      ESP_LOGE("wifiCommand", "error: (%d)", result);
    }
    esp_http_client_cleanup(client);
  } else {
    this->getSsl(url);
  }
}

void Wifi::getSsl(std::string& url){
  ESP_LOGI("getSsl", "enter");
  
  esp_tls_cfg_t cfg = {
    .crt_bundle_attach = esp_crt_bundle_attach,
  };
  std::unique_ptr buf = std::make_unique<char[]>(512);
  int ret;

  struct esp_tls* tls = esp_tls_conn_http_new(url.c_str(), &cfg);

  if(tls != nullptr){
    ESP_LOGI("getSsl", "Connection established...");
  } else {
    ESP_LOGE("getSsl", "Connection failed...");
    esp_tls_conn_destroy(tls);
    return;
  }
  
  std::string format = {"GET %s HTTP/1.1\r\nUser-Agent: esp-idf/1.0 esp32\r\n\r\n"};
  std::string::size_type len = std::snprintf(nullptr, 0, format.c_str(), url);
  std::string request;
  request.reserve(len);
  std::snprintf(request.data(), request.length(), format.c_str(), url);

  size_t written_bytes = 0;
  do {
    ret = esp_tls_conn_write(tls, request.c_str() + written_bytes, request.length() - written_bytes);
    if (ret >= 0) {
      ESP_LOGI("getSsl", "%d bytes written", ret);
      written_bytes += ret;
    } else if (ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
      ESP_LOGE("getSsl", "esp_tls_conn_write  returned: [0x%02X](%s)", ret, esp_err_to_name(ret));
      esp_tls_conn_destroy(tls);
      return;
    }
  } while (written_bytes < request.length());

  ESP_LOGI("getSsl", "Reading HTTP response...");

  do {
    len = sizeof(buf.get()) - 1;
    //std::memset(buf.get(), 0, sizeof(buf.get()));
    ret = esp_tls_conn_read(tls, buf.get(), len);
    if(ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ){
      continue;
    }

    if(ret < 0){
      ESP_LOGE("getSsl", "esp_tls_conn_read  returned [-0x%02X](%s)", -ret, esp_err_to_name(ret));
      break;
    }

    if(ret == 0){
      ESP_LOGI("getSsl", "connection closed");
      break;
    }

    len = ret;
    ESP_LOGD("getSsl", "%d bytes read", len);
    std::cout << buf.get();
  } while(true);
}
