#pragma once

class RequestHandler {
  public:
  virtual err_t handleRequest(std::string& request, std::any& args) = 0;
};
