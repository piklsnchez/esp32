#pragma once

#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/tcp.h>
#include <any>
#include <map>
#include <string>
#include "RequestHandler.hpp"

extern const char* Server_errorString[];

class Server {
  public:
  Server*   parent;
  int       clientCount;
  int       previousClientCount;
  RequestHandler* requestHandler;
  Server(RequestHandler* requestHandler);
  Server();
  virtual ~Server();
  struct tcp_pcb* pcb;
  virtual err_t start();
  virtual err_t stop();
  virtual err_t bind();
  virtual err_t listen();
  virtual err_t accept();
  virtual err_t doRequest(std::string& request);
};
