/*
 * Camera.h
 *
 *  Created on: Dec 29, 2021
 *      Author: chris
 */
#pragma once

#include <esp_camera.h>

class Camera {
  camera_fb_t* frameBuffer = nullptr;
  sensor_t*    sensor      = nullptr;
  public:
  Camera();
  camera_fb_t* takePicture();
  void freeFrameBuffer();
  void setWidth(int width);
  int  getWidth();
  void setHeight(int height);
  int  getHeight();
  void setJpegQuality(int jpegQuality);
  int  getJpegQuality();
};
