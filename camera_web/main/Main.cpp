#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <sdkconfig.h>
#include <esp_event.h>
#include <esp_log.h>
#include <nvs_flash.h>
#include <cstdio>
#include <iostream>
#include <string>
#include "Camera.hpp"
#include "Wifi.hpp"
#include "ImageServer.hpp"
#include "UdpClient.hpp"
#include "UdpServer.h"

static UdpClient* notifySender = nullptr;

void mainTask(void* arg){
  Wifi wifi = Wifi();
  wifi.connect();
  Camera      camera = Camera();
  ImageServer server = ImageServer(wifi, camera);
  server.startWebServer();
  if(nullptr == notifySender){
    notifySender = new UdpClient(const_cast<ip_addr_t*>IP_ADDR_BROADCAST, 1370);
  }
  int iteration = 0;
  while(true){
    //broadcast a message for the network to listen for
    /*char notifyMessage[100];
    std::snprintf(notifyMessage, sizeof(notifyMessage), "notify %d\n", iteration);
    notifySender->send(notifySender, notifyMessage);*/
    vTaskDelay(10000);
    iteration++;
  }
}

void statTask(void* arg){
  while(true){
    std::string statBuffer = std::string("", 1024);
    vTaskGetRunTimeStats(statBuffer.data());
    std::cout << statBuffer << std::endl;
    vTaskDelay(60000);
  }
}

extern "C" void app_main(){
  ESP_LOGI("app_main", "starting");
  
  xTaskCreate(mainTask, "mainTask", STACK_SIZE, NULL, 10, NULL);
  //xTaskCreate(statTask, "statTask", STACK_SIZE, NULL, 10, NULL);
}
