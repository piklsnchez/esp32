#ifndef __UDP_SERVER_H
#define __UDP_SERVER_H

#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/udp.h>

struct UdpServer{
  ip_addr_t* address;
  int port;
  struct udp_pcb* pcb;
  void (*free)(struct UdpServer* thiz);
  void (*receive)(struct UdpServer* thiz);
} typedef UdpServer_t;

UdpServer_t* UdpServer_new();
#endif
