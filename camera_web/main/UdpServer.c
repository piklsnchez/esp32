#include <stdbool.h>
#include <string.h>
#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/udp.h>
#include "UdpServer.h"

#define IP_SOF_BROADCAST 1
#define IP_SOF_BROADCAST_RECV 1

static void UdpServer_free(UdpServer_t* thiz){
  vPortFree(thiz->address);
  thiz->address = NULL;
  udp_disconnect(thiz->pcb);
  udp_remove(thiz->pcb);
  thiz->pcb = NULL;
  vPortFree(thiz);
}

static void UdpServer_receive_callback(void* _thiz, struct udp_pcb* pcb, struct pbuf* buffer, const ip_addr_t* address, u16_t port){
  printf("received udp packet: ...\n");
  pbuf_free(buffer);
}

static void UdpServer_receive(UdpServer_t* thiz){
  udp_recv(thiz->pcb, UdpServer_receive_callback, thiz);
}

//IP_ADDR_ANY
UdpServer_t* UdpServer_new(ip_addr_t* address, int port){
  UdpServer_t _thiz = {
    .address = pvPortMalloc(sizeof(ip_addr_t)),
    .port    = port,
    .pcb     = udp_new(),
    .free    = UdpServer_free,
    .receive = UdpServer_receive,
  };
  memcpy(_thiz.address, address, sizeof(ip_addr_t));

  ip_set_option(_thiz.pcb, SOF_BROADCAST);

  err_t result = udp_bind(_thiz.pcb, _thiz.address, _thiz.port);
  if(ERR_OK != result){
    fprintf(stderr, "ERROR: bind\n");
  }
  
  UdpServer_t* thiz = pvPortMalloc(sizeof(_thiz));
  memcpy(thiz, &_thiz, sizeof(_thiz));

  struct udp_pcb* pcb = thiz->pcb;
  do {
    udp_recv(pcb, UdpServer_receive_callback, thiz);
    printf("%s; %d; %s; %p; %p\n", ipaddr_ntoa(&pcb->local_ip), pcb->local_port, ipaddr_ntoa(&pcb->mcast_ip4), pcb->recv, pcb->recv_arg);
  } while(false && NULL != (pcb = pcb->next));

  return thiz;
}
