#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/tcp.h>
#include <esp_netif.h>
#include <esp_camera.h>
#include <esp_tls_crypto.h>
#include <esp_http_server.h>
#include <time.h>
#include <cstdio>
#include <iostream>
#include <any>
#include <string>
#include <sstream>
#include <memory>
#include "private/private.h"
#include "String.h"
#include "Server.hpp"
#include "Wifi.hpp"
#include "Camera.hpp"
#include "ImageServer.hpp"

static const int MAX_CONTENT_LENGTH   = 150;

static time_t randomTime;

static esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err){
	httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
	return ESP_FAIL;
}

static esp_err_t favicon_get_handler(httpd_req_t* req){
  ESP_LOGI("favicon_get_handler", "enter");
  
  //ImageServer* thiz = reinterpret_cast<ImageServer*>(req->user_ctx);
  httpd_resp_set_type(req, "text/html");
  httpd_resp_set_status(req, HTTPD_204);
  
  ESP_LOGI("favicon_get_handler", "return");
  return httpd_resp_send(req, "", 0);
}

static esp_err_t index_get_handler(httpd_req_t* req){
  ESP_LOGI("index_get_handler", "enter");

//  ImageServer* thiz = reinterpret_cast<ImageServer*>(req->user_ctx);
  httpd_resp_set_type(req, "text/html");
  const std::string html = "<!DOCTYPE html>"
	"<html>\n"
	"<head>\n"
	"<script type=\"text/javascript\">function doSubmit(form){\n"
	"  window.stop();\n"
	"  fetch(form.action, {method: form.method, headers: {\"Content-Type\": form.enctype}, body: `quality=${form.elements[\"quality\"].value}`});\n"
	"  let iframe = document.querySelector(\"iframe\");\n"
	"  iframe.src = iframe.src;\n"
	"}</script>\n"
	"</head>\n"
	"<body>\n"
	"  <div>\n"
	"    <form id=\"form\" enctype=\"text/plain\" action=\"/quality\" method=\"POST\" onsubmit=\"doSubmit(this); return false;\" >\n"
	"      <input type=\"number\" name=\"quality\"></input>\n"
	"      <input type=\"submit\">\n"
	"    </form>\n"
	"  </div>\n"
	"  <iframe src=\"/stream\" ></iframe>\n"
	"  <script>let iframe = document.querySelector(\"iframe\"); iframe.width=window.screen.width; iframe.height=window.screen.height;</script>"
	"</body>\n"
	"</html>";

//  int             stringLength = std::snprintf(nullptr, 0, htmlFormat.c_str(), thiz->camera.getWidth(), thiz->camera.getHeight());
//  std::unique_ptr htmlPtr      = std::make_unique<char[]>(stringLength);
 // std::snprintf(htmlPtr.get(), stringLength, htmlFormat.c_str(), thiz->camera.getWidth(), thiz->camera.getHeight());
  httpd_resp_send(req, html.c_str(), HTTPD_RESP_USE_STRLEN);
  
  ESP_LOGI("index_get_handler", "return");
  return ESP_OK;
}

static esp_err_t quality_post_handler(httpd_req_t* req){
  ESP_LOGI("quality_post_handler", "enter");
  
  ImageServer* thiz = reinterpret_cast<ImageServer*>(req->user_ctx);
  esp_err_t    res  = ESP_OK;
  int contentLength = req->content_len;
  
  if(MAX_CONTENT_LENGTH < contentLength){
    res = httpd_resp_set_status(req, "412 Payload Too Large");
    if(ESP_OK != res) return res;
  
    res = httpd_resp_set_type(req, HTTPD_TYPE_TEXT);
    if(ESP_OK != res) return res;
  
    res = httpd_resp_send(req, "The Payload Content Length is Too Large", HTTPD_RESP_USE_STRLEN);
    if(ESP_OK != res) return res;
    res = ESP_FAIL;
  }
  
  std::unique_ptr contentPtr = std::make_unique<char[]>(contentLength);
  httpd_req_recv(req, contentPtr.get(), contentLength);
  
  int quality;
  std::sscanf(contentPtr.get(), "quality=%d", &quality);
  thiz->camera.setJpegQuality(quality);
  
  puts(contentPtr.get());
  
  httpd_resp_set_type(req, "text/html");
  httpd_resp_set_status(req, HTTPD_204);
  
  ESP_LOGI("quality_post_handler", "return");
  return httpd_resp_send(req, "", 0);
}

static esp_err_t stream_get_handler(httpd_req_t* req){
  ESP_LOGI("stream_get_handler", "enter");
  
  std::cout << "req: " << req << "req.aux: " << req->aux << std::endl;
  ImageServer* thiz = reinterpret_cast<ImageServer*>(req->user_ctx);
  #define PART_BOUNDARY "123456789000000000000987654321"
  const char* _STREAM_CONTENT_TYPE = "multipart/x-mixed-replace;boundary=" PART_BOUNDARY;
  const char* _STREAM_BOUNDARY     = "\r\n--" PART_BOUNDARY "\r\n";
  const char* _STREAM_PART         = "Content-Type: image/jpeg\r\nContent-Length: %u\r\n\r\n";

  esp_err_t res = httpd_resp_set_type(req, _STREAM_CONTENT_TYPE);
  if(res != ESP_OK){
    return res;
  }

  while(true){
	camera_fb_t* frameBuffer = thiz->camera.takePicture();

    size_t          hlen    = std::snprintf(nullptr, 0, _STREAM_PART, frameBuffer->len);
    std::unique_ptr partPtr = std::make_unique<char[]>(hlen);
                              std::snprintf(partPtr.get(), hlen +1, _STREAM_PART, frameBuffer->len);
    
    res = httpd_resp_send_chunk(req, const_cast<const char*>(partPtr.get()), hlen);
    if(res != ESP_OK){
      std::cerr << "ERROR: sending chunk" << std::endl;
	  thiz->camera.freeFrameBuffer();
      break;
    }

    res = httpd_resp_send_chunk(req, reinterpret_cast<const char*>(frameBuffer->buf), frameBuffer->len);
    if(res != ESP_OK){
      std::cerr << "ERROR: sending framebuffer chunk" << std::endl;
      thiz->camera.freeFrameBuffer();
      break;
    }
    
    res = httpd_resp_send_chunk(req, _STREAM_BOUNDARY, strlen(_STREAM_BOUNDARY));
    if(res != ESP_OK){
      std::cerr << "ERROR: sending last chunk" << std::endl;
      thiz->camera.freeFrameBuffer();
      break;
    }

    thiz->camera.freeFrameBuffer();
    vTaskDelay(5);
  }
  return ESP_OK;
}

ImageServer::~ImageServer(){
}

ImageServer::ImageServer(Wifi &wifi, Camera &camera): wifi(wifi), camera(camera){
	srand((unsigned)time(&randomTime));
}

httpd_handle_t ImageServer::startWebServer(){
	httpd_config_t config   = HTTPD_DEFAULT_CONFIG();
	config.lru_purge_enable = true;

	// Start the httpd server
	ESP_LOGI("startWebServer", "Starting server on port: '%d'", config.server_port);

	if(httpd_start(&this->handle, &config) == ESP_OK){
		// Set URI handlers
		ESP_LOGI("startWebServer", "Registering URI handlers");
		httpd_register_err_handler(this->handle, HTTPD_404_NOT_FOUND, http_404_error_handler);
		
		const httpd_uri_t favicon = {
		  .uri      = "/favicon.ico",
		  .method   = HTTP_GET,
		  .handler  = favicon_get_handler,
		  .user_ctx = this,
		};
		httpd_register_uri_handler(this->handle, &favicon);

		const httpd_uri_t index = {
		  .uri      = "/",
		  .method   = HTTP_GET,
		  .handler  = index_get_handler,
		  .user_ctx = this,
		};
		httpd_register_uri_handler(this->handle, &index);

		const httpd_uri_t quality = {
		  .uri      = "/quality",
		  .method   = HTTP_POST,
		  .handler  = quality_post_handler,
		  .user_ctx = this,
		};
		httpd_register_uri_handler(this->handle, &quality);

		const httpd_uri_t stream = {
		  .uri      = "/stream",
		  .method   = HTTP_GET,
		  .handler  = stream_get_handler,
		  .user_ctx = this,
		};
		httpd_register_uri_handler(this->handle, &stream);
		
		return this->handle;
	}

	ESP_LOGI("startWebServer", "Error starting server!");
	return nullptr;
}

void ImageServer::stopWebServer(){
	httpd_stop(this->handle);
}
