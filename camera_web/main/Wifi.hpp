#pragma once

#include <freertos/event_groups.h>
#include <string>

class Wifi {
  public:
  char ipaddress[16];
  Wifi();
  void scan();
  void connect();
  void get(std::string& url);
  void getSsl(std::string& url);
};
