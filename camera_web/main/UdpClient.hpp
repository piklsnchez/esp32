#pragma once
#include <lwip/api.h>
#include <lwip/sys.h>
#include <lwip/udp.h>
#include <string>

struct UdpClient{
  ip_addr_t*      address;
  int             port;
  struct udp_pcb* pcb;
  UdpClient(ip_addr_t* address, int port);
  ~UdpClient();
  err_t send(std::string& message);
};
