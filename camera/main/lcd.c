#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "esp_log.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "esp_heap_caps.h"
#include "esp_camera.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "font.h"
#include "lcd.h"

static void drawFrameBuffer(lcd_t* this, camera_fb_t* frameBuffer){
  int horizontalWindowOffset = 4;
  esp_lcd_panel_draw_bitmap(this->handle, 0, 0 + horizontalWindowOffset, frameBuffer->width, frameBuffer->height + horizontalWindowOffset, frameBuffer->buf);
}

static void lcd_free(lcd_t* this){
  esp_lcd_panel_del(this->handle);
  vPortFree(this);
}

static bool transferComplete(esp_lcd_panel_io_handle_t panel_io, esp_lcd_panel_io_event_data_t* edata, void* user_ctx){
  lcd_t* lcd = (lcd_t*)user_ctx;
  xSemaphoreGive(lcd->drawingSemaphore);
  return true;
}

lcd_t* lcd_new(){
  ESP_LOGI("lcd_new", "enter");
  lcd_t _this = {
    .CHAR_HEIGHT           = 8,
    .CHAR_WIDTH            = 5,
    .HOST                  = SPI2_HOST,
    .PIXEL_CLOCK_HZ        = 10 * 1000 * 1000,
    .BACKLIGHT_OFF         = 1,
    .MOSI_PIN              = 13,
    .CLOCK_PIN             = 14,
    .CS_PIN                = 15,
    .DC_PIN                = 2,
    .RESET_PIN             = 12,
    .HORIZONTAL_RESOLUTION = 160,
    .VERTICAL_RESOLUTION   = 128,
    .CMD_BITS              = 8,
    .PARAM_BITS            = 8,
    .drawingSemaphore      = xSemaphoreCreateBinary(),
    .drawFrameBuffer       = drawFrameBuffer,
    .free                  = lcd_free,
  };
  lcd_t* this = pvPortMalloc(sizeof(lcd_t));
  memcpy(this, &_this, sizeof(lcd_t));
  //setup spi
  spi_bus_config_t spi_config = {
    .sclk_io_num     = this->CLOCK_PIN,
    .mosi_io_num     = this->MOSI_PIN,
    .miso_io_num     = -1,
    .quadwp_io_num   = -1,
    .quadhd_io_num   = -1,
    .max_transfer_sz = 81928, //arbitrary, keep this big or error
  };
  ESP_ERROR_CHECK(spi_bus_initialize(this->HOST, &spi_config, SPI_DMA_CH_AUTO));
  ESP_LOGI("lcd_new", "spi inited");
  //io
  esp_lcd_panel_io_handle_t io_handle = NULL;
  esp_lcd_panel_io_spi_config_t io_config = {
    .dc_gpio_num         = this->DC_PIN,
    .cs_gpio_num         = this->CS_PIN,
    .pclk_hz             = this->PIXEL_CLOCK_HZ,
    .lcd_cmd_bits        = this->CMD_BITS,
    .lcd_param_bits      = this->PARAM_BITS,
    .spi_mode            = 0,
    .trans_queue_depth   = 10,
    .on_color_trans_done = transferComplete,
    .user_ctx            = this,
  };
  //spi
  ESP_ERROR_CHECK(esp_lcd_new_panel_io_spi((esp_lcd_spi_bus_handle_t)this->HOST, &io_config, &io_handle));
  ESP_LOGI("lcd_new", "panel spi inited");
  //panel
  esp_lcd_panel_dev_config_t panel_config = {
    .reset_gpio_num = this->RESET_PIN,
    .color_space    = ESP_LCD_COLOR_SPACE_RGB,
    .bits_per_pixel = 16,
  };
  ESP_ERROR_CHECK(esp_lcd_new_panel_st7789(io_handle, &panel_config, &this->handle));
  ESP_LOGI("lcd_new", "panel configured");
  //reset, init
  ESP_ERROR_CHECK(esp_lcd_panel_reset(this->handle));
  ESP_ERROR_CHECK(esp_lcd_panel_init(this->handle));
  ESP_LOGI("lcd_new", "lcd inited");
  ESP_ERROR_CHECK(esp_lcd_panel_swap_xy(this->handle, true));
  ESP_ERROR_CHECK(esp_lcd_panel_mirror(this->handle, true, false));
  int size = this->HORIZONTAL_RESOLUTION * this->VERTICAL_RESOLUTION * sizeof(uint16_t);
  void* buffer = pvPortMalloc(size);
  memset(buffer, 0, size);
  esp_lcd_panel_draw_bitmap(this->handle, 0, 0, this->HORIZONTAL_RESOLUTION, this->VERTICAL_RESOLUTION, buffer);
  vPortFree(buffer);
//  xSemaphoreTake(this->drawingSemaphore, portMAX_DELAY);
  return this;
}
