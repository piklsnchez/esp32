#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "lcd.h"
#include "camera.h"
#include "main.h"

lcd_t*    lcd    = NULL;
camera_t* camera = NULL;

void consoleTask(void* args){
  ESP_LOGI("consoleTask", "starting");
  char command[55];
  while(true){
    command[0] = 0;
    gets(command);
    if(0 == strlen(command)){
      vTaskDelay(1);
      continue;
    }
    ESP_LOGI("consoleTask", "%s", command);
    TaskHandle_t commandTaskHandle;
    xTaskCreate(commandTask, "command_task", STACK_SIZE, NULL, 10, &commandTaskHandle);
  }
  vTaskDelete(NULL);
}

void commandTask(void* commandArg){
  char* arg = (char*)commandArg;
//  if(0 == strcmp("camera", arg)){
    cameraCommand(arg);
//  }
  vTaskDelete(NULL);
}

void cameraCommand(char* arg){
  //do camera stuff
  if(NULL == lcd){
    lcd = lcd_new();
    if(NULL == lcd){
      ESP_LOGE("cameraCommand", "failed to init lcd");
      return;
    }
  }
  if(NULL == camera){
    camera = camera_new(lcd);
    if(NULL == camera){
      ESP_LOGE("cameraCommand", "initing camera failed");
      return;
    }
  }
//  camera->takePicture(camera);
  camera->startCapture(camera);
}

void app_main(){
  ESP_LOGI("app_main", "starting");
  TaskHandle_t consoleTaskHandle;
  xTaskCreate(consoleTask, "console_task", STACK_SIZE, NULL, 10, &consoleTaskHandle);
}
