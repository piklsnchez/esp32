/*
 * camera.h
 *
 *  Created on: Dec 29, 2021
 *      Author: chris
 */

#ifndef MAIN_CAMERA_H_
#define MAIN_CAMERA_H_
#include "esp_camera.h"
#include "lcd.h"

struct camera {
  lcd_t*           lcd;
  camera_config_t* config;
  camera_fb_t*     frameBuffer;
  void (*free)(struct camera* this);
  void (*takePicture)(struct camera* this);
  void (*startCapture)(struct camera* this);
  void (*freeFrameBuffer)(struct camera* this);
} typedef camera_t;

camera_t* camera_new(lcd_t* lcd);

#endif /* MAIN_CAMERA_H_ */
